import argparse
import h5py
import os
import shutil
import time
import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.nn.parallel
import torch.backends.cudnn as cudnn
import torch.optim
import torch.utils.data
import torchvision.transforms as transforms
import torchvision.datasets as datasets
import numpy as np
from spiking import loadPretrainedModel


from model import mnist_mlp, mnist_mlp_2
from train_routines import *

n_bits = 4

def createMNISTForBNN():
    batch_size = 1
    data_transform = transforms.Compose([
                           transforms.ToTensor(),
                           transforms.Normalize(mean=[0.1307], std=[0.3081])
                           ])
    train_loader = torch.utils.data.DataLoader(datasets.MNIST('./data', train=True, download=True,
                       transform=data_transform),batch_size=batch_size, shuffle=True)

    outFName = "savedData/mnistForSim.h5"
    gpName = "mnist"
    hf = h5py.File(outFName, 'a')
    g = hf.create_group(gpName)

    for i, (input, target) in enumerate(train_loader):
        if i == 20000: # saving only 20k training images out of 60k
            break
        x = input.numpy().reshape((batch_size, 784))
        dsName = "img-" + str(i)
        g.create_dataset(dsName, data=x)


def L1HingeLoss(top_output,labels,hinge = 2**n_bits):
    loss = F.relu(top_output - top_output[torch.arange(top_output.size(0)).long(),labels].unsqueeze(1) + hinge)
    return loss.sum(1).mean()

def main():
    data_transform = transforms.Compose([
                           transforms.ToTensor(),
                           lambda x: torch.round(torch.clamp(x,0,1))
                           ])
    batch_size=1
    train_loader = torch.utils.data.DataLoader(datasets.MNIST('./data', train=True, download=True,
                       transform=data_transform),batch_size=batch_size, shuffle=True)
    val_loader = torch.utils.data.DataLoader(datasets.MNIST('./data', train=False, transform=data_transform), batch_size=batch_size, shuffle=False)

    use_vanilla_network = False
    n_hidden = 2
    n_neurons = 600
    n_bits = 4
    bipolar_activations = False
    ternary_errors = True
    model = mnist_mlp(use_vanilla_network, n_hidden, n_neurons, n_bits, bipolar_activations, ternary_errors, 0.1, 0.3)
    use_cuda = False
    #model = mnist_mlp_2(use_vanilla_network, n_hidden, n_neurons, n_bits, bipolar_activations, ternary_errors, 0.1, 0.3)
    # Note: mnist_mlp_2 needs to be TRAINED in order for it to work
    print(model)

    lr = 1
    criterion =  L1HingeLoss
    optimizer = torch.optim.SGD(model.parameters(), lr)

    fileNm = 'mnist_chkpt85.680.tar'
    checkpoint = torch.load('./data/checkpoints/' + fileNm)
    model.load_state_dict(checkpoint['model_state_dict'])
    optimizer.load_state_dict(checkpoint['optimizer_state_dict'])
    epoch = checkpoint['epoch']
    loss = checkpoint['loss']

    #print(model._modules.keys())
    temp = model._modules.get('layer_stack')
    print (temp._modules.keys())

    # model is in eval mode now
    model.eval()

########### Extract info from mnist_mlp #################
    l0_out = temp._modules.get('0') # Save output of layer-0 (input)
    l1_out = temp._modules.get('1') # Save output of layer-1
    l2_out = temp._modules.get('2') # Save output of layer-2
    l3_out = temp._modules.get('3') # Save output of layer-3
    l4_out = temp._modules.get('4') # Save output of layer-4
    l5_out = temp._modules.get('5') # Save output of layer-5
    l6_out = temp._modules.get('6') # Save output of layer-6
    l7_out = temp._modules.get('7') # Save output of layer-7

    my_embedding = torch.zeros((batch_size, 784)) # copy of l0 out
    l1_out_copy = torch.zeros((batch_size, 600)) # copy of l1 out
    l2_out_copy = torch.zeros((batch_size, 600)) # copy of l2 out
    l3_out_copy = torch.zeros((batch_size, 600)) # copy of l3 out
    l4_out_copy = torch.zeros((batch_size, 600)) # copy of l4 out
    l5_out_copy = torch.zeros((batch_size, 600)) # copy of l5 out
    l6_out_copy = torch.zeros((batch_size, 600)) # copy of l6 out
    l7_out_copy = torch.zeros((batch_size, 10)) # copy of l7 out

    def get_ftrs_l0(m, i, o): # function to copy output of l0
        my_embedding.copy_(o.data)

    def get_ftrs_l1(m, i, o): # function to copy output of l1
        l1_out_copy.copy_(o.data)

    def get_ftrs_l2(m, i, o): # function to copy output of l2
        l2_out_copy.copy_(o.data)

    def get_ftrs_l3(m, i, o): # function to copy output of l3
        l3_out_copy.copy_(o.data)

    def get_ftrs_l4(m, i, o): # function to copy output of l4
        l4_out_copy.copy_(o.data)

    def get_ftrs_l5(m, i, o): # function to copy output of l5
        l5_out_copy.copy_(o.data)

    def get_ftrs_l6(m, i, o): # function to copy output of l6
        l6_out_copy.copy_(o.data)

    def get_ftrs_l7(m, i, o): # function to copy output of l7
        l7_out_copy.copy_(o.data)

    h0 = l0_out.register_forward_hook(get_ftrs_l0)
    h1 = l1_out.register_forward_hook(get_ftrs_l1)
    h2 = l2_out.register_forward_hook(get_ftrs_l2)
    h3 = l3_out.register_forward_hook(get_ftrs_l3)
    h4 = l4_out.register_forward_hook(get_ftrs_l4)
    h5 = l5_out.register_forward_hook(get_ftrs_l5)
    h6 = l6_out.register_forward_hook(get_ftrs_l6)
    h7 = l7_out.register_forward_hook(get_ftrs_l7)

    out = torch.zeros((batch_size, 10))
    for i, (input, tgt) in enumerate(val_loader):
        #print (input.numpy().shape)
        #np.savetxt('input1.txt', input.numpy()[1,0,:,:], fmt='%1.2f')
        print (tgt.numpy().shape)
        np.savetxt('mnist_class_labels.txt', tgt.numpy()[:], fmt='%d')
        out = model(input)
        break

    #np.savetxt('data_l0.txt', my_embedding, fmt='%1.2f')
    #np.savetxt('l1_out.txt', l1_out_copy, fmt='%1.2f')
    #np.savetxt('l2_out.txt', l2_out_copy, fmt='%1.2f')
    #np.savetxt('l3_out.txt', l3_out_copy, fmt='%1.2f')
    #np.savetxt('l4_out.txt', l4_out_copy, fmt='%1.2f')
    #np.savetxt('l5_out.txt', l5_out_copy, fmt='%1.2f')
    #np.savetxt('l6_out.txt', l6_out_copy, fmt='%1.2f')
    #np.savetxt('last_layer_out.txt', l7_out_copy, fmt='%1.2f')

    #print(out)

########### Extract info from mnist_mlp (END) #################

    '''
    print("Model's state_dict:")
    for param_tensor in model.state_dict():
        print(param_tensor, "\t", model.state_dict()[param_tensor].size())

    # Print optimizer's state_dict
    print("Optimizer's state_dict:")
    for var_name in optimizer.state_dict():
            print(var_name, "\t", optimizer.state_dict()[var_name])
    '''

    '''
    hf = h5py.File('binary_mnist_mlp_wts.h5', 'a')
    g = hf.create_group('model_weights')
    i = 0
    for param in model.parameters():
        wts = param.data.numpy()
        bias = np.zeros((wts.shape[0]))
        print ('size of wts: ', wts.shape, ' size of bias: ', bias.shape)
        gpName = g.create_group('fc'+str(i+1))
        gpName1 = gpName.create_group('fc'+str(i+1))
        gpName1.create_dataset('kernel', data=np.transpose(wts))
        gpName1.create_dataset('bias', data=bias)
        print("max: ", np.amax(param.data.numpy()), " min: ", np.amin(param.data.numpy()))
        i+=1
    #print(list(model.parameters()))
    #print(list(model.parameters())[0].data.numpy())
    '''

    print ('size of val loader: {}'.format(val_loader.__len__()))
    avg_prec, avg_loss = validate(val_loader, model, criterion, epoch,use_cuda)
    print ('avg_prec: ', avg_prec, ' avg_loss: ', avg_loss)

def createH5Wts(model, h5FileNm):
    hf = h5py.File(h5FileNm, 'a')
    g = hf.create_group('model_weights')
    i = 0
    for param in model.parameters():
        wts = param.data.numpy()
        bias = np.zeros((wts.shape[0]))
        print ('size of wts: ', wts.shape, ' size of bias: ', bias.shape)
        gpName = g.create_group('fc'+str(i+1))
        gpName1 = gpName.create_group('fc'+str(i+1))
        gpName1.create_dataset('kernel', data=np.transpose(wts))
        gpName1.create_dataset('bias', data=bias)
        print("max: ", np.amax(param.data.numpy()), " min: ", np.amin(param.data.numpy()))
        i+=1

def debugWts(model):
    i = 0
    for param in model.parameters():
        wts = param.data.numpy()
        bias = np.zeros((wts.shape[0]))
        #wts = np.transpose(wts)
        print ('size of wts: ', wts.shape, ' size of bias: ', bias.shape)
        print('Saving neuron weights for layer {}'.format(i))
        fnm = 'bnn_spiking_wts_layer-' + str(i) + '.txt'
        np.savetxt(fnm, wts, fmt='%d')
        i += 1
        #for n in range(wts.shape[1]):

def createH5Input(inFName, outFName, gpName, dsName, inData, loadFromFile=True, mode='r'):
    #data = np.zeros((1, 784))
    if loadFromFile:
        data = np.loadtxt(inFName)
    else:
        data = inData
    hf = h5py.File(outFName, mode)
    g = hf.create_group(gpName)
    #temp = np.zeros((1, 784))
    temp = np.zeros(data.shape)
    #temp[0, :] = data[0, :]
    g.create_dataset(dsName, data=temp)

if __name__ == '__main__':
    #main()

    #createH5Input(inFName, outFName, gpName, dsName)

    '''
    model_path = 'data/spiking_bnn_checkpoints_actual_errors'
    fileNm = 'spiking_bnn_94.220.pth'
    model = loadPretrainedModel(model_path, fileNm)
    h5FileNm = 'spiking_mnist_mlp_wts.h5'
    createH5Wts(model, h5FileNm)
    model_path = 'data/spiking_bnn_checkpoints_actual_errors'
    fileNm = 'spiking_bnn_94.220.pth'
    model = loadPretrainedModel(model_path, fileNm)
    debugWts(model)
    '''
    createMNISTForBNN()


