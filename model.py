import torch
import torch.nn as nn
from integer_base import *
#from spiking import SpikeRelu

class mnist_mlp(nn.Module):

    def __init__(self, vanilla_mlp, n_hidden_layers, neurons_per_layer, nbits, bipolar, ternary_error,input_dropout_prob,inter_layer_dropout_prob):
        super(mnist_mlp, self).__init__()

        layer_list = []
        nonLinearity = nn.ReLU() if vanilla_mlp else BinaryActivationFactory(bipolar,ternary_error,2**nbits,1.0e-3)

        for i in range(n_hidden_layers + 1):
            input_dim = 784 if i==0 else neurons_per_layer
            output_dim = 10 if i==n_hidden_layers else neurons_per_layer
            max_float = 0.5#2.0 if i==n_hidden_layers else 0.5
            inter_layer_dropout_prob = 0.1
            layer_list.append(nn.Dropout(p = input_dropout_prob if i==0 else inter_layer_dropout_prob))
            if vanilla_mlp:
                layer_list.append(nn.Linear(input_dim,output_dim,bias=False))
            else:
                layer_list.append(IntLinear(input_dim,output_dim,nbits,max_float))

            if i != n_hidden_layers:
                layer_list.append(nonLinearity)

        self.layer_stack = nn.Sequential(*layer_list)

    def integerize(self):
        [x.integerize() for x in self.modules() if isinstance(x,IntTensor)]

    def forward(self, x):
        return self.layer_stack(x.view(-1,784))


class lenet5(nn.Module):

    def __init__(self):
        super(lenet5, self).__init__()

        # set-1
        self.conv1 = nn.Conv2d(in_channels=1, out_channels=20, kernel_size=5, padding=2, bias=False)
        self.bin1 = nn.ReLU()
        self.avg_pool1 = nn.AvgPool2d(kernel_size=2, stride=2)

        # set-2
        self.conv2 = nn.Conv2d(in_channels=20, out_channels=50, kernel_size=5, padding=2, bias=False)
        self.bin2 = nn.ReLU()
        self.avg_pool2 = nn.AvgPool2d(kernel_size=2, stride=2)

        # set-3
        self.fc3 = nn.Linear(50*7*7, 500, bias=False)
        self.bin3 = nn.ReLU()

        # set-4
        self.fc4 = nn.Linear(500, 10, bias=False)

        # intermediate output
        self.conv_relu1 = torch.zeros(1)
        self.avg1 = torch.zeros(1)
        self.conv_relu2 = torch.zeros(1)
        self.avg2 = torch.zeros(1)
        self.fc_relu1 = torch.zeros(1)
        self.last_layer = torch.zeros(1)

        self.conv_relu1 = self.conv_relu1.cuda()
        self.avg1 = self.avg1.cuda()
        self.conv_relu2 = self.conv_relu2.cuda()
        self.avg2 = self.avg2.cuda()
        self.fc_relu1 = self.fc_relu1.cuda()
        self.last_layer = self.last_layer.cuda()

    def forward(self, x):
        x = self.conv1(x)
        x = self.bin1(x)
        #self.conv_relu1 = torch.max(x, self.conv_relu1) # output of 1st conv-relu layer
        self.conv_relu1 = x

        x = self.avg_pool1(x)
        self.avg1 = torch.max(x, self.avg1) # output of 1st avg-pool layer

        x = self.conv2(x)
        x = self.bin2(x)
        self.conv_relu2 = torch.max(x, self.conv_relu2) # output of 2nd conv-relu layer

        x = self.avg_pool2(x)
        self.avg2 = torch.max(x, self.avg2) # output of 2nd avg-pool layer

        x = x.view(-1, 7*7*50)
        x = self.fc3(x)
        x = self.bin3(x)
        self.fc_relu1 = torch.max(x, self.fc_relu1)

        x = self.fc4(x)
        self.last_layer = torch.max(x, self.last_layer)

        return x

    def print_max_act(self):
        print ('max activations: ')
        print ('conv_relu1: {:f}, avg1: {:f}, conv_relu2: {:f}, avg2: {:f}, fc_relu1: {:f}, fc2: {:f}' \
                .format(torch.max(self.conv_relu1), torch.max(self.avg1), torch.max(self.conv_relu2), torch.max(self.avg2), \
                torch.max(self.fc_relu1), torch.max(self.last_layer)))

    def get_act_val(self, index):
        return self.conv_relu1[index]


class mnist_mlp_2(nn.Module):

    def __init__(self, vanilla_mlp, n_hidden_layers, neurons_per_layer, nbits, bipolar, ternary_error,input_dropout_prob,inter_layer_dropout_prob):
        super(mnist_mlp_2, self).__init__()

        nonLinearity = nn.ReLU() if vanilla_mlp else BinaryActivationFactory(bipolar,ternary_error,2**nbits,1.0e-3)
        max_float = 0.5

        input_dim = 784
        output_dim = neurons_per_layer
        self.drop1 = nn.Dropout(p = input_dropout_prob)
        self.intLin1 = IntLinear(input_dim, output_dim, nbits, max_float)
        self.bin1 = nonLinearity

        input_dim = neurons_per_layer
        self.drop2 = nn.Dropout(p = inter_layer_dropout_prob)
        self.intLin2 = IntLinear(input_dim, output_dim, nbits, max_float)
        self.bin2 = nonLinearity

        output_dim = 10
        self.drop3 = nn.Dropout(p = inter_layer_dropout_prob)
        self.intLin3 = IntLinear(input_dim, output_dim, nbits, max_float)

    def integerize(self):
        [x.integerize() for x in self.modules() if isinstance(x,IntTensor)]

    def forward(self, x):
        x = self.drop1(x.view(-1, 784))
        x = self.intLin1(x)
        x = self.bin1(x)

        x = self.drop2(x)
        x = self.intLin2(x)
        x = self.bin2(x)

        x = self.drop3(x)
        x = self.intLin3(x)

        return x
