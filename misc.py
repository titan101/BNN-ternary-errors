from spiking_lenet5 import saveData
import numpy as np
import torch

time_window = 2
batch_size = 4
num_batches = 1
(C, H, W) = 3, 4, 4
outFileNm = 'savedData/dummy.h5'

for n in range(num_batches):
    for t in range(time_window):
        data = np.multiply(np.arange(batch_size*C*H*W), 1-t*time_window)
        data = np.reshape(data, (batch_size,C,H,W))
        saveData(outFileNm, data, t)

