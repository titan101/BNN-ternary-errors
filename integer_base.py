import torch
import torch.nn as nn
import torch.nn as nn
from torch.nn.parameter import Parameter
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
import torch.utils.data
import torchvision
import numpy as np
import math

class IntTensor(nn.Module):
    def __init__(self,tensor_size,nbits,max_float):
        super(IntTensor,self).__init__()
        self.int_tensor = Parameter(torch.Tensor(torch.Size(tensor_size)))

        self.nbits = nbits
        self.max_float = max_float
        self.max_int = 2**(nbits-1) - 1 
        
        self.init_parameters()
     
    def init_parameters(self):
        receptive_field_size = np.prod(self.int_tensor.size()[2:])
        stdv = math.sqrt(2/ ((self.int_tensor.size(0) + self.int_tensor.size(1)) * receptive_field_size))
        
        self.int_tensor.data.uniform_(-stdv, stdv).mul_(self.max_int).div_(self.max_float)
        self.integerize()

    def integerize(self):
        self.int_tensor.data = torch.round(torch.clamp(self.int_tensor.data,-self.max_int,self.max_int))

    def forward(self):
        return self.int_tensor

    def extra_repr(self):
        return 'tensor size : {} , nbits ={}'.format(
            self.int_tensor.size(), self.nbits)
    

class IntLinear(nn.Module):

    def __init__(self, in_features, out_features,nbits,max_float):
        super(IntLinear, self).__init__()
        self.in_features = in_features
        self.out_features = out_features
        self.i_tensor = IntTensor([out_features,in_features],nbits,max_float)


    def init_parameters(self):
        self.i_tensor.init_parameters()
            
    def forward(self, input):
        return F.linear(input, self.i_tensor() , None)

def BinaryActivationFactory(bipolar,ternary_error,input_grad_cutoff,grad_deadzone):
    class BinaryActivation(torch.autograd.Function):
        @staticmethod
        def forward(ctx, x):
            ctx.save_for_backward(x)
            if bipolar:
               res =  ((x > 0).float() - (x <= 0).float())
            else:
               res =  (x > 0).float()
            return res 

        @staticmethod
        def backward(ctx, grad_out):
            x, = ctx.saved_tensors
            filtered_grad = grad_out * (x > -input_grad_cutoff).float() * (x < input_grad_cutoff).float()
            if ternary_error:
                filtered_grad  = ((filtered_grad > grad_deadzone).float() - (filtered_grad < -grad_deadzone).float())

            return filtered_grad
    activation = BinaryActivation.apply
    class BinaryActivation_m(nn.Module):
        def forward(self,x):
            return activation(x)
        def extra_repr(self):
            return 'bipolar : {}, ternary : {}, input_grad_cutoff : {}, grad_deadzone : {}'.format(bipolar,ternary_error,input_grad_cutoff,grad_deadzone)
    return BinaryActivation_m()
