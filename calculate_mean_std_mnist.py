# for computing mean and std of training data to be used for test data

import torch
import torchvision
import torchvision.transforms as transforms
import numpy as np

def main():
    transform = transforms.Compose([transforms.ToTensor()])
    trainset = torchvision.datasets.MNIST(root='./data', train=True, download=True, transform=transform)
    trainloader = torch.utils.data.DataLoader(trainset, batch_size=10, shuffle=False, num_workers=0)

    mean = 0.0
    for images, _ in trainloader:
        batch_samples = images.size(0)
        images = images.view(batch_samples, images.size(1), -1)
        mean += images.mean(2).sum(0)

    mean = mean / len(trainloader.dataset)
    np.savetxt('mnist_mean.txt', mean)

    var = 0.0
    for images, _ in trainloader:
        batch_samples = images.size(0)
        images = images.view(batch_samples, images.size(1), -1)
        var += ((images - mean.unsqueeze(1))**2).sum([0,2])

    std = torch.sqrt(var / (len(trainloader.dataset)*28*28))
    np.savetxt('mnist_std.txt', std)

    print (mean)
    print (std)

if __name__ == '__main__':
    main()

