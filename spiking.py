import numpy as np
import os
import torch
import torch.utils.data
import torchvision.transforms as transforms
import torchvision.datasets as datasets
from model import mnist_mlp, lenet5
import torch.nn as nn
import datetime

def SpikeSoftmax():
    class spikeSoftmax(nn.Module):

        def __init__(self):
            super(spikeSoftmax, self).__init__()
            self.vmem = 0
            self.time = 0 # internal timer to keep track of simulation time

        def forward(self, x):
            self.vmem += x

            self.time += 1
            return self.vmem

        def extra_repr(self):
            return 'vmem : {}'.format(self.vmem)

    return spikeSoftmax()

### clamping supported
def SpikeRelu(v_th, layer_num=None, clp_slp=None):
    class spikeRelu(nn.Module):

        def __init__(self, v_th, layer_num=0, clp_slp=0):
            super(spikeRelu, self).__init__()
            self.threshold = v_th
            self.layer_num = layer_num
            self.d = clp_slp

            self.vmem = 0
            self.clamp_time = layer_num * clp_slp

        def forward(self, x):
            self.clamp_time -= 1

            if self.clamp_time < 0:
                # integrate the sum(w_i*x_i)
                self.vmem += x

            # generate output spikes
            op_spikes = torch.where(self.vmem >= self.threshold, torch.ones(1), torch.zeros(1))

            # vmem reset
            # 'reset-to-zero'
            #self.vmem = torch.where(self.vmem >= self.threshold, torch.zeros(1), self.vmem)

            # 'reset-to-threshold'
            self.vmem = torch.where(self.vmem >= self.threshold, self.vmem-self.threshold, self.vmem)

            return op_spikes

        def extra_repr(self):
            return 'v_th : {}, vmem : {}'.format(v_th, self.vmem)

    return spikeRelu(v_th)


''' clamping not supported
def SpikeRelu(v_th, monitor=False, index=None):
    class spikeRelu(nn.Module):

        def __init__(self, v_th, monitor=monitor, index=index):
            super(spikeRelu, self).__init__()
            self.threshold = v_th
            self.vmem = 0
            self.time = 0 # internal timer to keep track of simulation time
            self.index = index
            self.monitor = monitor
            self.spike_train = []

        def forward(self, x):
            self.vmem += x
            index = self.index

            #if self.monitor == True:
            #    print('input {}\tvmem {}'.format(x[index], self.vmem[index]))

            self.inter = self.vmem
            op_spikes = torch.where(self.vmem >= self.threshold, torch.ones(1), torch.zeros(1))
            # 'reset-to-zero'
            #self.vmem = torch.where(self.vmem >= self.threshold, torch.zeros(1), self.vmem)
            # note: changing the vmem update equation to 'reset-to-threshold'
            self.vmem = torch.where(self.vmem >= self.threshold, self.vmem-self.threshold, self.vmem)

            if self.monitor == True:
                #print(x)
                #print('input {}\tvmem {}'.format(x[index], self.vmem[index]))
                #print(op_spikes)
                #print(op_spikes[index])
                self.spike_train.append(op_spikes[index])


            self.time += 1
            return op_spikes

        def extra_repr(self):
            return 'v_th : {}, vmem : {}'.format(v_th, self.vmem)

        def get_spike_train(self):
            #print (len(self.spike_train))
            return (self.inter[self.index], self.spike_train)

    return spikeRelu(v_th)
'''


n_bits = 8
def L1HingeLoss(top_output,labels,hinge = 2**n_bits):
    loss = F.relu(top_output - top_output[torch.arange(top_output.size(0)).long(),labels].unsqueeze(1) + hinge)
    return loss.sum(1).mean()

# rand_val corresponds to vmem
# in_val/max_in_val corresponds to threshold
def condition(rand_val, in_val, abs_max_val):
    if rand_val <= (abs(in_val) / abs_max_val):
        return (np.sign(in_val))
    else:
        return 0

def poisson_spikes(pixel_vals):
    out_spikes = np.zeros(pixel_vals.shape)
    for b in range(pixel_vals.shape[0]):
        random_inputs = np.random.rand(pixel_vals.shape[1],pixel_vals.shape[2], pixel_vals.shape[3])
        single_img = pixel_vals[b,:,:,:]
        #max_val = np.amax(single_img) # note: shouldn't this be max(abs(single_img)) ??
        max_val = np.amax(abs(single_img)) # note: shouldn't this be max(abs(single_img)) ??
        vfunc = np.vectorize(condition)
        out_spikes[b,:,:,:] = vfunc(random_inputs, single_img, max_val)
    return out_spikes

def loadPretrainedModel(model_path, fileNm):
    #use_vanilla_network = True
    use_vanilla_network = False
    n_hidden = 2
    n_neurons = 600
    bipolar_activations = False
    ternary_errors = False
    lr = 0.01
    model = mnist_mlp(use_vanilla_network, n_hidden, n_neurons, n_bits, bipolar_activations, ternary_errors, 0.1, 0.1)
    criterion =  L1HingeLoss
    optimizer = torch.optim.SGD(model.parameters(), lr)

    checkpoint = torch.load(os.path.join(model_path, fileNm))
    model.load_state_dict(checkpoint['model_state_dict'])
    #print ('Original model')
    #print (model)
    optimizer.load_state_dict(checkpoint['optimizer_state_dict'])
    epoch = checkpoint['epoch']
    loss = checkpoint['loss']

    temp = model._modules.get('layer_stack')
    #print (temp._modules.keys())

    # model is in eval mode now
    model.eval()

    return model

def createSpikingModel(model_path, fileNm):
    #tensor(63.2367) tensor(265.6114) tensor(3770.5779)

    #thresholds = [10, 20, 30]
    model_original = loadPretrainedModel(model_path, fileNm)
    spike_temp = model_original

    temp = spike_temp._modules.get('layer_stack')
    temp._modules['2'] = SpikeRelu(1/2.82)
    temp._modules['5'] = SpikeRelu(1/1)
    #temp._modules['2'] = SpikeRelu(232.57/2.82)
    #temp._modules['5'] = SpikeRelu(232.57/232.57)
    spikingModel = nn.Sequential(
            list(temp),
            #SpikeRelu(922/1)
            SpikeRelu(865/1)
            )
    #spikingModel = nn.Sequential(
    #        *list(temp),
    #        #SpikeRelu(922/1)
    #        SpikeRelu(865/1)
    #        )
    print (spikingModel)
    return spikingModel


def computeThresholds(model_path, fileNm, batch_size):
    data_transform = transforms.Compose([
                           transforms.ToTensor(),
                           transforms.Normalize(mean=[0.1307], std=[0.3081])
                           ])
    batch_size = 10000
    val_loader = torch.utils.data.DataLoader(datasets.MNIST('./data', train=False, transform=data_transform), batch_size=batch_size, shuffle=False)
    #val_loader = torch.utils.data.DataLoader(datasets.MNIST('./data', train=True, transform=data_transform), batch_size=batch_size, shuffle=False)

    model = loadPretrainedModel(model_path, fileNm)
    print (model)
    #return
    temp = model._modules.get('layer_stack')

    ########### Extract info from mnist_mlp #################
    l0_out = temp._modules.get('0') # Save output of layer-0
    l2_out = temp._modules.get('2') # Save output of layer-2
    l5_out = temp._modules.get('5') # Save output of layer-5
    l7_out = temp._modules.get('7') # Save output of layer-7

    l0_out_copy = torch.zeros((batch_size, 784)) # copy of l0 out
    l2_out_copy = torch.zeros((batch_size, 600)) # copy of l2 out
    l5_out_copy = torch.zeros((batch_size, 600)) # copy of l5 out
    l7_out_copy = torch.zeros((batch_size, 10)) # copy of l7 out

    def get_ftrs_l0(m, i, o): # function to copy output of l0
        l0_out_copy.copy_(o.data)

    def get_ftrs_l2(m, i, o): # function to copy output of l2
        l2_out_copy.copy_(o.data)

    def get_ftrs_l5(m, i, o): # function to copy output of l5
        l5_out_copy.copy_(o.data)

    def get_ftrs_l7(m, i, o): # function to copy output of l7
        l7_out_copy.copy_(o.data)

    h0 = l0_out.register_forward_hook(get_ftrs_l0)
    h2 = l2_out.register_forward_hook(get_ftrs_l2)
    h5 = l5_out.register_forward_hook(get_ftrs_l5)
    h7 = l7_out.register_forward_hook(get_ftrs_l7)

    dataset_size = 60000
    max_act_l2 = np.zeros((dataset_size))
    print('Predicting the category for all test images..')
    total_correct = 0
    total_images = 0
    batch_num = 0
    confusion_matrix = np.zeros([10,10], int)
    outputs = []
    labels_np = np.zeros((batch_size, 10))
    with torch.no_grad():
        for data in val_loader:
            if batch_num == 1:
                break
            images, labels = data
            #labels_np = labels.numpy()
            images_np = images.numpy()
            outputs = model(images)
            l2_out_np = l2_out_copy.numpy()
            #print(batch_num*batch_size, (batch_num+1)*batch_size, l2_out_np.shape)

            max_act_l2[batch_num*batch_size: (batch_num+1)*batch_size] = np.amax(l2_out_np, axis=1)
            _, predicted = torch.max(outputs.data, 1)
            total_images += labels.size(0)
            total_correct += (predicted == labels).sum().item()
            for i, l in enumerate(labels):
                confusion_matrix[l.item(), predicted[i].item()] += 1
            #if batch_num >= 1:
            #    break
            batch_num += 1
    model_accuracy = total_correct / total_images * 100
    print('Model accuracy on {0} test images: {1:.2f}%'.format(total_images, model_accuracy))

    #np.savetxt('bnn_ll_out_train.txt', outputs, fmt='%1.2f')
    #np.savetxt('bnn_fl_in_train.txt', l0_out_copy, fmt='%1.5f')
    np.savetxt('bnn_fl_in_test.txt', l0_out_copy, fmt='%1.5f')
    #np.savetxt('bnn_l2out_train.txt', l2_out_copy, fmt='%1.5f')
    #np.savetxt('bnn_l7out_train.txt', l7_out_copy, fmt='%1.2f')
    #np.savetxt('labels_train.txt', labels_np, fmt='%d')

    '''
    np.savetxt('max_act_l2.txt', max_act_l2, fmt='%1.2f')
    sortedAct = np.sort(max_act_l2)
    print(sortedAct)
    np.savetxt('sortedAct.txt', sortedAct, fmt='%1.2f')
    print('99th percentile {}\t99.99th percentile {}'.format(sortedAct[59400], sortedAct[59994]))
    '''
    l2_max = 0
    l5_max = 0
    l7_max = 0
    l2_max = max(l2_max, torch.max(l2_out_copy))
    l5_max = max(l5_max, torch.max(l5_out_copy))
    l7_max = max(l7_max, torch.max(l7_out_copy))
    print (l2_max, l5_max, l7_max)

## Note: the normalization values are: tensor(1.) tensor(1.) tensor(856.)



def extractNeuronalActivity(spikeModel):
    model = spikeModel
    #temp = model._modules.get('layer_stack')
    temp = model
    l2_out = temp._modules.get('2') # Save output of layer-2
    l5_out = temp._modules.get('5') # Save output of layer-5
    l8_out = temp._modules.get('8') # Save output of layer-7

    l2_out_copy = torch.zeros((batch_size, 600)) # copy of l2 out
    l5_out_copy = torch.zeros((batch_size, 600)) # copy of l5 out
    l8_out_copy = torch.zeros((batch_size, 10)) # copy of l7 out

    def get_ftrs_l2(m, i, o): # function to copy output of l2
        l2_out_copy.copy_(o.data)

    def get_ftrs_l5(m, i, o): # function to copy output of l5
        l5_out_copy.copy_(o.data)

    def get_ftrs_l8(m, i, o): # function to copy output of l7
        l8_out_copy.copy_(o.data)

    h2 = l2_out.register_forward_hook(get_ftrs_l2)
    h5 = l5_out.register_forward_hook(get_ftrs_l5)
    h8 = l8_out.register_forward_hook(get_ftrs_l8)

    fileNm1 = 'neural_activity_l' + str(2) + '.txt'
    fileNm2 = 'neural_activity_l' + str(5) + '.txt'
    fileNm3 = 'neural_activity_l' + str(8) + '.txt'
    np.savetxt(fileNm1, l2_out_copy, fmt='%1.5f')
    np.savetxt(fileNm2, l5_out_copy, fmt='%1.5f')
    np.savetxt(fileNm3, l8_out_copy, fmt='%1.5f')


def simulateSpikeModel(model_path, fileNm):
    data_transform = transforms.Compose([
                           transforms.ToTensor(),
                           transforms.Normalize(mean=[0.1307], std=[0.3081])
                           ])
    batch_size = 1
    val_loader = torch.utils.data.DataLoader(datasets.MNIST('./data', train=False, transform=data_transform), batch_size=batch_size, shuffle=False)


    #time_window = 20 # (lenet-5)snn accuracy: 91.40%, ann acc: 95% (2.4)
    #time_window = 4 # bnn spiking acc: 93.44%, non-spiking acc: 94% (when last layer was non-spiking)
    time_window = 10 # bnn spiking acc: 93.0%, non-spiking acc: 94% (when last layer was spiking)
    #time_window = 50 # bnn (full prec)spiking acc: 96.53%, non-spiking acc: 96.160%
    total_correct = 0
    total_images = 0
    batch_num = 0
    confusion_matrix = np.zeros([10,10], int)
    out_spikes_t_b_c = torch.zeros((time_window, batch_size, 10))
    tt, numBatches = 0, int(10000 / batch_size)
    in_spikes_t_i_np = np.zeros((numBatches*time_window, 784))
    out_spikes_t_i_np = np.zeros((numBatches*time_window, 10))

    # supply previously recorded inputs
    #in_spikes_t_i_np = np.loadtxt('spike_bin_mlp_in.txt')
    print('in_spikes_t_i_np shape: {}'.format(in_spikes_t_i_np.shape))
    #spikeModel = createSpikingModel(model_path, fileNm) #NOTE: instantiating the spike model just once

    l2_out_sum = torch.zeros((batch_size, 600)) # copy of l2 out
    l5_out_sum = torch.zeros((batch_size, 600)) # copy of l5 out
    l8_out_sum = torch.zeros((batch_size, 10)) # copy of l8 out

    with torch.no_grad():
        for data in val_loader:
            if batch_num >= numBatches:
                break
            print ('\n\n------------ inferring batch {} -------------'.format(batch_num))
            images, labels = data
            #print(torch.max(images, dim=1))
            spikeModel = createSpikingModel(model_path, fileNm) #NOTE: instantiating the spike model for a new batch of data
            #extractNeuronalActivity(spikeModel)
            ####################### extract neuronal activity at every layer ##############
            temp = spikeModel
            l2_out = temp._modules.get('2') # Save output of layer-2
            l5_out = temp._modules.get('5') # Save output of layer-5
            l8_out = temp._modules.get('8') # Save output of layer-8

            l2_out_copy = torch.zeros((batch_size, 600)) # copy of l2 out
            l5_out_copy = torch.zeros((batch_size, 600)) # copy of l5 out
            l8_out_copy = torch.zeros((batch_size, 10)) # copy of l8 out

            def get_ftrs_l2(m, i, o): # function to copy output of l2
                l2_out_copy.copy_(o.data)

            def get_ftrs_l5(m, i, o): # function to copy output of l5
                l5_out_copy.copy_(o.data)

            def get_ftrs_l8(m, i, o): # function to copy output of l8
                l8_out_copy.copy_(o.data)

            h2 = l2_out.register_forward_hook(get_ftrs_l2)
            h5 = l5_out.register_forward_hook(get_ftrs_l5)
            h8 = l8_out.register_forward_hook(get_ftrs_l8)

            ####################### extract neuronal activity at every layer (end) ##############

            #spikeModel = createSpikingModel_generic(model_path, fileNm)
            #for param in spikeModel.parameters():
            #    wts = param.data.numpy()
            #    print ('wt val, max {}\tmin {}'.format(np.amax(wts), np.amin(wts)))
            for t in range(time_window):
                #print('shape of input data:', images.shape)

                # supply previously recorded inputs
                #spikes = in_spikes_t_i_np[batch_num*time_window + t,:] # note: assuming batch-size=1

                # convert image pixels to spiking inputs
                spikes = poisson_spikes(images.numpy())

                # supply random inputs
                spikes = torch.from_numpy(spikes)

                #out_spikes = spikeModel(spikes.float())
                out_spikes = spikeModel(spikes.reshape((batch_size, 784)).float())
                #print(out_spikes)
                out_spikes_t_b_c[t,:,:] = out_spikes

                l2_out_sum += l2_out_copy
                l5_out_sum += l5_out_copy
                l8_out_sum += l8_out_copy

                ########## collecting info for simulator
                in_spikes_t_i_np[tt,:] = spikes.reshape((batch_size, 784))
                out_spikes_t_i_np[tt,:] = out_spikes.numpy()
                #print ('tt: {}'.format(tt))
                tt += 1
                ########## collecting info for simulator (end)

            # accumulating output spikes for all images in a batch
            total_spikes_b_c = torch.zeros((batch_size, 10))
            for b in range(batch_size):
                total_spikes_per_input = torch.zeros((10))
                for t in range(time_window):
                    total_spikes_per_input += out_spikes_t_b_c[t,b,:]
                #print ("total spikes per output: {}".format(total_spikes_per_input / time_window))
                print ("total spikes per output: {}".format(total_spikes_per_input ))
                total_spikes_b_c[b,:] = total_spikes_per_input
                #total_spikes_b_c[b,:] = total_spikes_per_input / time_window # note the change

            _, predicted = torch.max(total_spikes_b_c.data, 1)
            total_images += labels.size(0)
            total_correct += (predicted == labels).sum().item()
            for i, l in enumerate(labels):
                confusion_matrix[l.item(), predicted[i].item()] += 1
            batch_num += 1
    model_accuracy = total_correct / total_images * 100
    print('Model accuracy on {0} test images: {1:.2f}%'.format(total_images, model_accuracy))

    dirNm = 'savedData'
    fileNm1 = 'neural_activity_l' + str(2) + '.csv'
    fileNm2 = 'neural_activity_l' + str(5) + '.csv'
    fileNm3 = 'neural_activity_l' + str(8) + '.csv'
    import pandas as pd
    data1 = l2_out_sum / (time_window*numBatches)
    data2 = l5_out_sum / (time_window*numBatches)
    data3 = l8_out_sum / (time_window*numBatches)
    data1 = data1.numpy()
    data2 = data2.numpy()
    data3 = data3.numpy()

    pd.DataFrame(np.reshape(data1, (data1.shape[1])), \
            columns=['activity']).to_csv(os.path.join(dirNm, fileNm1))
            #columns=['neuron num', 'activity']).to_csv(os.path.join(dirNm, fileNm1))
    pd.DataFrame(np.reshape(data2, (data2.shape[1])), \
            columns=['activity']).to_csv(os.path.join(dirNm, fileNm2))
            #columns=['neuron num', 'activity']).to_csv(os.path.join(dirNm, fileNm2))
    pd.DataFrame(np.reshape(data3, (data3.shape[1])), \
            columns=['activity']).to_csv(os.path.join(dirNm, fileNm3))
            #columns=['neuron num', 'activity']).to_csv(os.path.join(dirNm, fileNm3))

    #np.savetxt(fileNm1, l2_out_sum / (time_window*numBatches), fmt='%1.5f', delimiter='\n')
    #np.savetxt(fileNm2, l5_out_sum / (time_window*numBatches), fmt='%1.5f', delimiter='\n')
    #np.savetxt(fileNm3, l8_out_sum / (time_window*numBatches), fmt='%1.5f', delimiter='\n')

    #print (l2_out_sum )
    #print (l5_out_sum )
    #print (l8_out_sum )

    #print (l2_out_sum / (time_window*numBatches))
    #print (l5_out_sum / (time_window*numBatches))
    #print (l8_out_sum / (time_window*numBatches))

    ######### collecting info for simulator
    #np.savetxt('spike_bin_mlp_in_all.txt', in_spikes_t_i_np, fmt='%d')
    #np.savetxt('spike_bin_mlp_out_all.txt', out_spikes_t_i_np, fmt='%d')
    ######### collecting info for simulator (end)


def main():
    model_path = 'data/spiking_bnn_checkpoints_actual_errors'
    fileNm = 'spiking_bnn_94.220.pth'
    #model_path = 'data/spiking_float'
    #fileNm = 'spiking_bnn_96.160.pth'
    #dummy_input = 2*np.random.rand(784) - np.ones(784)
    #print ('shape of input: {}. max value of input: {}. min value of input: {}'.format(dummy_input.shape, np.amax(dummy_input), np.amin(dummy_input)))
    #time_window = 2
    #poisson_spikes(time_window, dummy_input)

    batch_size = 50
    #computeThresholds(model_path, fileNm, batch_size)

    simulateSpikeModel(model_path, fileNm)

if __name__ == '__main__':
    main()
