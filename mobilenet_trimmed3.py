from math import floor
import torch
import torch.nn as nn
from spiking import SpikeRelu

device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")

class MobileNet_trimmed3(nn.Module):
    def __init__(self):
        super(MobileNet_trimmed3, self).__init__()

        #size = 3,56,56 (stride=2)
        self.conv1 = nn.Conv2d(in_channels=3, out_channels=32, kernel_size=3, stride=2, padding=1, groups=1, bias=False)
        self.relu1 = nn.ReLU(inplace=True)
        self.bn1 = nn.BatchNorm2d(32)
        # ofm = 32,28,28

        # dw-conv-1 (stride=1)
        self.conv2 = nn.Conv2d(in_channels=32, out_channels=32, kernel_size=3, stride=1, padding=1, groups=32, bias=False)
        self.relu2 = nn.ReLU(inplace=True)
        self.bn2 = nn.BatchNorm2d(32)
        # ofm = 32,28,28
        self.conv3 = nn.Conv2d(in_channels=32, out_channels=64, kernel_size=1, stride=1, padding=0, groups=1, bias=False)
        self.relu3 = nn.ReLU(inplace=True)
        self.bn3 = nn.BatchNorm2d(64)
        # ofm = 64,28,28

        # dw-conv-2 (stride=2)
        self.conv4 = nn.Conv2d(in_channels=64, out_channels=64, kernel_size=3, stride=2, padding=1, groups=64, bias=False)
        self.relu4 = nn.ReLU(inplace=True)
        self.bn4 = nn.BatchNorm2d(64)
        # ofm = 64,14,14
        self.conv5 = nn.Conv2d(in_channels=64, out_channels=128, kernel_size=1, stride=1, padding=0, groups=1, bias=False)
        self.relu5 = nn.ReLU(inplace=True)
        self.bn5 = nn.BatchNorm2d(128)
        # ofm = 128,14,14

        # dw-conv-3 (stride=1)
        self.conv6 = nn.Conv2d(in_channels=128, out_channels=128, kernel_size=3, stride=1, padding=1, groups=128, bias=False)
        self.relu6 = nn.ReLU(inplace=True)
        self.bn6 = nn.BatchNorm2d(128)
        # ofm = 128,14,14
        self.conv7 = nn.Conv2d(in_channels=128, out_channels=256, kernel_size=1, stride=1, padding=0, groups=1, bias=False)
        self.relu7 = nn.ReLU(inplace=True)
        self.bn7 = nn.BatchNorm2d(256)
        # ofm = 256,14,14

        # dw-conv-4 (stride=2)
        self.conv8 = nn.Conv2d(in_channels=256, out_channels=256, kernel_size=3, stride=2, padding=1, groups=256, bias=False)
        self.relu8 = nn.ReLU(inplace=True)
        self.bn8 = nn.BatchNorm2d(256)
        # ofm = 256,7,7
        self.conv9 = nn.Conv2d(in_channels=256, out_channels=512, kernel_size=1, stride=1, padding=0, groups=1, bias=False)
        self.relu9 = nn.ReLU(inplace=True)
        self.bn9 = nn.BatchNorm2d(512)
        # ofm = 512,7,7

        # dw-conv-5 (stride=1)
        self.conv10 = nn.Conv2d(in_channels=512, out_channels=512, kernel_size=3, stride=1, padding=1, groups=512, bias=False)
        self.relu10 = nn.ReLU(inplace=True)
        self.bn10 = nn.BatchNorm2d(512)
        # ofm = 512,7,7
        self.conv11 = nn.Conv2d(in_channels=512, out_channels=1024, kernel_size=1, stride=1, padding=0, groups=1, bias=False)
        self.relu11 = nn.ReLU(inplace=True)
        self.bn11 = nn.BatchNorm2d(1024)
        # ofm = 1024,7,7

        self.avgpool1 = nn.AvgPool2d(7)
        # ofm = 1024,1,1

        self.fc1 = nn.Linear(1024, 200, bias=False)
        # ofm = 200

    def forward(self, x):
        x = self.conv1(x)
        x = self.relu1(x)
        #self.conv_relu1 = x
        x = self.bn1(x)
        self.conv_relu_bn1 = x

        x = self.conv2(x)
        x = self.relu2(x)
        x = self.bn2(x)

        x = self.conv3(x)
        x = self.relu3(x)
        x = self.bn3(x)

        x = self.conv4(x)
        x = self.relu4(x)
        x = self.bn4(x)

        x = self.conv5(x)
        x = self.relu5(x)
        x = self.bn5(x)

        x = self.conv6(x)
        x = self.relu6(x)
        x = self.bn6(x)

        x = self.conv7(x)
        x = self.relu7(x)
        x = self.bn7(x)

        x = self.conv8(x)
        x = self.relu8(x)
        x = self.bn8(x)

        x = self.conv9(x)
        x = self.relu9(x)
        x = self.bn9(x)

        x = self.conv10(x)
        x = self.relu10(x)
        x = self.bn10(x)

        x = self.conv11(x)
        x = self.relu11(x)
        x = self.bn11(x)

        x = self.avgpool1(x)

        x = x.view(-1, 1024)
        x = self.fc1(x)

        return x

    def get_act_val(self, index):
        return self.conv_relu_bn1[index]

class MobileNet_trimmed3_nobn(nn.Module):
    def __init__(self):
        super(MobileNet_trimmed3_nobn, self).__init__()

        #size = 3,56,56 (stride=2)
        self.conv1 = nn.Conv2d(in_channels=3, out_channels=32, kernel_size=3, stride=2, padding=1, groups=1)
        self.relu1 = nn.ReLU(inplace=True)
        # ofm = 32,28,28

        # dw-conv-1 (stride=1)
        self.conv2 = nn.Conv2d(in_channels=32, out_channels=32, kernel_size=3, stride=1, padding=1, groups=32)
        self.relu2 = nn.ReLU(inplace=True)
        # ofm = 32,28,28
        self.conv3 = nn.Conv2d(in_channels=32, out_channels=64, kernel_size=1, stride=1, padding=0, groups=1)
        self.relu3 = nn.ReLU(inplace=True)
        # ofm = 64,28,28

        # dw-conv-2 (stride=2)
        self.conv4 = nn.Conv2d(in_channels=64, out_channels=64, kernel_size=3, stride=2, padding=1, groups=64)
        self.relu4 = nn.ReLU(inplace=True)
        # ofm = 64,14,14
        self.conv5 = nn.Conv2d(in_channels=64, out_channels=128, kernel_size=1, stride=1, padding=0, groups=1)
        self.relu5 = nn.ReLU(inplace=True)
        # ofm = 128,14,14

        # dw-conv-3 (stride=1)
        self.conv6 = nn.Conv2d(in_channels=128, out_channels=128, kernel_size=3, stride=1, padding=1, groups=128)
        self.relu6 = nn.ReLU(inplace=True)
        # ofm = 128,14,14
        self.conv7 = nn.Conv2d(in_channels=128, out_channels=256, kernel_size=1, stride=1, padding=0, groups=1)
        self.relu7 = nn.ReLU(inplace=True)
        # ofm = 256,14,14

        # dw-conv-4 (stride=2)
        self.conv8 = nn.Conv2d(in_channels=256, out_channels=256, kernel_size=3, stride=2, padding=1, groups=256)
        self.relu8 = nn.ReLU(inplace=True)
        # ofm = 256,7,7
        self.conv9 = nn.Conv2d(in_channels=256, out_channels=512, kernel_size=1, stride=1, padding=0, groups=1)
        self.relu9 = nn.ReLU(inplace=True)
        # ofm = 512,7,7

        # dw-conv-5 (stride=1)
        self.conv10 = nn.Conv2d(in_channels=512, out_channels=512, kernel_size=3, stride=1, padding=1, groups=512)
        self.relu10 = nn.ReLU(inplace=True)
        # ofm = 512,7,7
        self.conv11 = nn.Conv2d(in_channels=512, out_channels=1024, kernel_size=1, stride=1, padding=0, groups=1)
        self.relu11 = nn.ReLU(inplace=True)
        # ofm = 1024,7,7

        self.avgpool1 = nn.AvgPool2d(7)
        # ofm = 1024,1,1

        self.fc1 = nn.Linear(1024, 200, bias=False)
        # ofm = 200

    def forward(self, x):
        x = self.conv1(x)
        x = self.relu1(x)
        self.conv_relu1 = x

        x = self.conv2(x)
        x = self.relu2(x)

        x = self.conv3(x)
        x = self.relu3(x)

        x = self.conv4(x)
        x = self.relu4(x)

        x = self.conv5(x)
        x = self.relu5(x)

        x = self.conv6(x)
        x = self.relu6(x)

        x = self.conv7(x)
        x = self.relu7(x)

        x = self.conv8(x)
        x = self.relu8(x)

        x = self.conv9(x)
        x = self.relu9(x)

        x = self.conv10(x)
        x = self.relu10(x)

        x = self.conv11(x)
        x = self.relu11(x)

        x = self.avgpool1(x)

        x = x.view(-1, 1024)
        x = self.fc1(x)

        return x

    def get_act_val(self, index):
        return self.conv_relu1[index]

class MobileNet_trimmed3_spiking(nn.Module):
    def __init__(self, thresholds):
    #def __init__(self, thresholds, device):
        super(MobileNet_trimmed3_spiking, self).__init__()

        index = (0,0,20,20)
        #size = 3,56,56 (stride=2)
        self.conv1 = nn.Conv2d(in_channels=3, out_channels=32, kernel_size=3, stride=2, padding=1, groups=1, bias=False)
        self.relu1 = SpikeRelu(thresholds[0])
        #self.relu1 = SpikeRelu(thresholds[0], monitor=True, index=index)
        self.bn1 = nn.BatchNorm2d(32)
        self.bn1 = self.bn1.to(device)
        self.relu2 = SpikeRelu(thresholds[1], monitor=True, index=index)
        # ofm = 32,28,28

        # dw-conv-1 (stride=1)
        self.conv2 = nn.Conv2d(in_channels=32, out_channels=32, kernel_size=3, stride=1, padding=1, groups=32, bias=False)
        self.relu3 = SpikeRelu(thresholds[2])
        self.bn2 = nn.BatchNorm2d(32)
        self.bn2 = self.bn2.to(device)
        self.relu4 = SpikeRelu(thresholds[3])
        # ofm = 32,28,28
        self.conv3 = nn.Conv2d(in_channels=32, out_channels=64, kernel_size=1, stride=1, padding=0, groups=1, bias=False)
        self.relu5 = SpikeRelu(thresholds[4])
        self.bn3 = nn.BatchNorm2d(64)
        self.bn3 = self.bn3.to(device)
        self.relu6 = SpikeRelu(thresholds[5])
        # ofm = 64,28,28

        # dw-conv-2 (stride=2)
        self.conv4 = nn.Conv2d(in_channels=64, out_channels=64, kernel_size=3, stride=2, padding=1, groups=64, bias=False)
        self.relu7 = SpikeRelu(thresholds[6])
        self.bn4 = nn.BatchNorm2d(64)
        self.bn4 = self.bn4.to(device)
        self.relu8 = SpikeRelu(thresholds[7])
        # ofm = 64,14,14
        self.conv5 = nn.Conv2d(in_channels=64, out_channels=128, kernel_size=1, stride=1, padding=0, groups=1, bias=False)
        self.relu9 = SpikeRelu(thresholds[8])
        self.bn5 = nn.BatchNorm2d(128)
        self.bn5 = self.bn5.to(device)
        self.relu10 = SpikeRelu(thresholds[9])
        # ofm = 128,14,14

        # dw-conv-3 (stride=1)
        self.conv6 = nn.Conv2d(in_channels=128, out_channels=128, kernel_size=3, stride=1, padding=1, groups=128, bias=False)
        self.relu11 = SpikeRelu(thresholds[10])
        self.bn6 = nn.BatchNorm2d(128)
        self.bn6 = self.bn6.to(device)
        self.relu12 = SpikeRelu(thresholds[11])
        # ofm = 128,14,14
        self.conv7 = nn.Conv2d(in_channels=128, out_channels=256, kernel_size=1, stride=1, padding=0, groups=1, bias=False)
        self.relu13 = SpikeRelu(thresholds[12])
        self.bn7 = nn.BatchNorm2d(256)
        self.bn7 = self.bn7.to(device)
        self.relu14 = SpikeRelu(thresholds[13])
        # ofm = 256,14,14

        # dw-conv-4 (stride=2)
        self.conv8 = nn.Conv2d(in_channels=256, out_channels=256, kernel_size=3, stride=2, padding=1, groups=256, bias=False)
        self.relu15 = SpikeRelu(thresholds[14])
        self.bn8 = nn.BatchNorm2d(256)
        self.bn8 = self.bn8.to(device)
        self.relu16 = SpikeRelu(thresholds[15])
        # ofm = 256,7,7
        self.conv9 = nn.Conv2d(in_channels=256, out_channels=512, kernel_size=1, stride=1, padding=0, groups=1, bias=False)
        self.relu17 = SpikeRelu(thresholds[16])
        self.bn9 = nn.BatchNorm2d(512)
        self.bn9 = self.bn9.to(device)
        self.relu18 = SpikeRelu(thresholds[17])
        # ofm = 512,7,7

        # dw-conv-5 (stride=1)
        self.conv10 = nn.Conv2d(in_channels=512, out_channels=512, kernel_size=3, stride=1, padding=1, groups=512, bias=False)
        self.relu19 = SpikeRelu(thresholds[18])
        self.bn10 = nn.BatchNorm2d(512)
        self.bn10 = self.bn10.to(device)
        self.relu20 = SpikeRelu(thresholds[19])
        # ofm = 512,7,7
        self.conv11 = nn.Conv2d(in_channels=512, out_channels=1024, kernel_size=1, stride=1, padding=0, groups=1, bias=False)
        self.relu21 = SpikeRelu(thresholds[20])
        self.bn11 = nn.BatchNorm2d(1024)
        self.bn11 = self.bn11.to(device)
        self.relu22 = SpikeRelu(thresholds[21])
        # ofm = 1024,7,7

        self.avgpool1 = nn.AvgPool2d(7)
        self.relu23 = SpikeRelu(thresholds[22])
        # ofm = 1024,1,1

        self.fc1 = nn.Linear(1024, 200, bias=False)
        self.relu24 = SpikeRelu(thresholds[23])
        # ofm = 200

    def forward(self, x):
        x = self.conv1(x)
        x = self.relu1(x)
        x = self.bn1(x)
        x = self.relu2(x)

        x = self.conv2(x)
        x = self.relu3(x)
        x = self.bn2(x)
        x = self.relu4(x)

        x = self.conv3(x)
        x = self.relu5(x)
        x = self.bn3(x)
        x = self.relu6(x)

        x = self.conv4(x)
        x = self.relu7(x)
        x = self.bn4(x)
        x = self.relu8(x)

        x = self.conv5(x)
        x = self.relu9(x)
        x = self.bn5(x)
        x = self.relu10(x)

        x = self.conv6(x)
        x = self.relu11(x)
        x = self.bn6(x)
        x = self.relu12(x)

        x = self.conv7(x)
        x = self.relu13(x)
        x = self.bn7(x)
        x = self.relu14(x)

        x = self.conv8(x)
        x = self.relu15(x)
        x = self.bn8(x)
        x = self.relu16(x)

        x = self.conv9(x)
        x = self.relu17(x)
        x = self.bn9(x)
        x = self.relu18(x)

        x = self.conv10(x)
        x = self.relu19(x)
        x = self.bn10(x)
        x = self.relu20(x)

        x = self.conv11(x)
        x = self.relu21(x)
        x = self.bn11(x)
        x = self.relu22(x)

        x = self.avgpool1(x)
        x = self.relu23(x)

        x = x.view(-1, 1024)
        x = self.fc1(x)
        x = self.relu24(x)

        return x

    def get_spike_train(self):
        return self.relu1.get_spike_train()
