import argparse
import os
import shutil
import time
import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.nn.parallel
import torch.backends.cudnn as cudnn
import torch.optim
import torch.utils.data
import torchvision.transforms as transforms
import torchvision.datasets as datasets
import numpy as np
from model import lenet5
import h5py
from torch.autograd import Variable

def float_to_fixed(val, W=2, F=6):
    ''' converts floating point number val to fixed format W.F
    using method described in http://ee.sharif.edu/~asic/Tutorials/Fixed-Point.pdf '''
    nearest_int = round(val*(2**F))
    return nearest_int*(1.0/2**F)


def quantize_model(model_path, quant_model_path, W=2, F=6):
    model = lenet5()
    model.load_state_dict(torch.load(model_path))
    model.eval()
    print(model)

    quantized_model = lenet5()
    quant_fn_vec = np.vectorize(float_to_fixed)
    all_wts = []

    for param in model.parameters():
        wts = param.data.numpy()
        print (wts.shape)
        if len(wts.shape) > 1:
            all_wts.append(quant_fn_vec(wts, W, F))

    print('size of all_wts: {}'.format(len(all_wts)))

    l = 0
    for layer in quantized_model.children():
        if isinstance(layer, nn.Conv2d) or isinstance(layer, nn.Linear):
            layer.weight.data = torch.from_numpy(all_wts[l])
            l += 1

    print('Quantized model, saving the model to {}'.format(quant_model_path))
    torch.save(quantized_model.state_dict(), quant_model_path)


def evaluate(model_path):
    data_transform = transforms.Compose([ transforms.ToTensor(),
        transforms.Normalize(mean=[0.1307], std=[0.3081]) ])
    batch_size = 100
    val_loader = torch.utils.data.DataLoader(datasets.MNIST('./data', train=False, transform=data_transform), batch_size=batch_size, shuffle=False)

    model = lenet5()

    if torch.cuda.is_available():
        model.cuda()
        use_cuda = True
    else:
        use_cuda = False

    model.load_state_dict(torch.load(model_path))
    model.eval()
    print(model)
    i = 0
    for param in model.parameters():
        wts = param.data.numpy()
        print("max: ", np.amax(wts), " min: ", np.amin(wts))
        print('{} {}'.format(i, param.data.size()))
        i += 1


    print('Predicting the category for all test images..')
    total_correct = 0
    total_images = 0
    confusion_matrix = np.zeros([10,10], int)
    with torch.no_grad():
        for data in val_loader:
            images, labels = data
            outputs = model(images)
            _, predicted = torch.max(outputs.data, 1)
            total_images += labels.size(0)
            total_correct += (predicted == labels).sum().item()
            for i, l in enumerate(labels):
                confusion_matrix[l.item(), predicted[i].item()] += 1

    model_accuracy = total_correct / total_images * 100
    print('Model accuracy on {0} test images: {1:.2f}%'.format(total_images, model_accuracy))


def train_lenet5(model_path):
    data_transform = transforms.Compose([ transforms.ToTensor(),
        transforms.Normalize(mean=[0.1307], std=[0.3081]) ])

    batch_size = 100
    train_loader = torch.utils.data.DataLoader(datasets.MNIST('./data', train=True, transform=data_transform, download=True), batch_size=batch_size, shuffle=True)

    val_loader = torch.utils.data.DataLoader(datasets.MNIST('./data', train=False, transform=data_transform), batch_size=batch_size, shuffle=False)

    model = lenet5()
    print(model)

    # Loss fn and optimizer
    criterion = nn.CrossEntropyLoss()
    optimizer = torch.optim.SGD(model.parameters(), lr=0.01, momentum=0.9)

    # Train the net
    epochs = 20
    log_interval=200
    for epoch in range(epochs):
        for batch_idx, data in enumerate(train_loader, 0):
            # get the inputs
            inputs, labels = data

            # zero the parameter gradients
            optimizer.zero_grad()

            # forward + backward + optimize
            outputs = model(inputs)
            loss = criterion(outputs, labels)
            loss.backward()
            optimizer.step()

            # print statistics
            if batch_idx % log_interval == 0:
                print('Train Epoch: {} [{}/{} ({:.0f}%)]\tLoss: {:.6f}'.format(
                    epoch, batch_idx * len(data), len(train_loader.dataset),
                    100. * batch_idx / len(train_loader), loss.data))

    print('Finished Training, saving the model to {}'.format(model_path))
    torch.save(model.state_dict(), model_path)

def createH5Wts(model, h5FileNm):
    hf = h5py.File(h5FileNm, 'a')
    g = hf.create_group('model_weights')
    i = 0
    for child in model.children():
        print (child)
        for param in child.parameters():
            print ('{} {}'.format(i, param.data.size()))

            wts = param.data.numpy()
            bias = np.zeros((wts.shape[0]))

            if (len(wts.shape) > 1):
                if isinstance(child, nn.Conv2d):
                    groupNm = 'block'+str(i+1)+'_conv'
                elif isinstance(child, nn.Linear):
                    groupNm = 'block'+str(i+1)+'_fc'

                gp = g.create_group(groupNm)
                gp1 = gp.create_group(groupNm)

                if isinstance(child, nn.Conv2d):
                    gp1.create_dataset('kernel', data=wts)
                elif isinstance(child, nn.Linear):
                    gp1.create_dataset('kernel', data=np.transpose(wts))

                gp1.create_dataset('bias', data=bias)
                i += 1

if __name__ == '__main__':

    '''
    model_path = './saved_models/lenet5/lenet5.pth' # test acc: 99.12%
    train_lenet5(model_path)

    evaluate(model_path)
    #quant_model_path = './saved_models/lenet5/lenet5_2.6.pth'
    quant_model_path = './saved_models/lenet5/lenet5_2.4.pth' # acc: 97.77%
    quantize_model(model_path, quant_model_path, 2, 4)
    evaluate(quant_model_path)
    '''

    from spiking_lenet5 import computeThresholds_generic
    model_path = './saved_models/lenet5'
    fileNm = 'lenet5_2.4.pth'
    computeThresholds_generic(model_path, fileNm)
    #max activations: conv_relu1: 15.130967, avg1: 14.167399, conv_relu2: 38.394875, avg2: 28.443781, fc_relu1: 21.573858, fc2: 33.420769

    #model = lenet5()
    #out_h5_file = './saved_models/lenet5_weights.h5'
    #createH5Wts(model, out_h5_file)

