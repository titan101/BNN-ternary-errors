import numpy as np
import matplotlib
import matplotlib.pyplot as plt
matplotlib.style.use('ggplot')

num_batches = 200
num_layers = 13
all_data = np.zeros((num_layers, num_batches))
for n in range(num_batches):
    a = np.load('correlation_trimmed5/correlation_data_'+str(n)+'.npy')
    all_data[:][n] = a
#all_data = np.load('correlation_trimmed5/correlation_data_'+str(9)+'.npy')

for l in range(num_layers):
    plt.plot(all_data[l])

leg = [str(i) for i in range(num_layers)]
plt.legend(leg)
plt.title('Correlation between ANN and SNN activations')
plt.show()

'''
import pandas as pd
import matplotlib.pyplot as plt
import os

#dirNm = 'savedData'
#stageNums = [2, 5, 8]
dirNm = '/i3c/hpcl/sms821/Research/SpikSim/simulator/SpikingSimAugust/src/inputs/spiking_mnist_mlp_exp2.3'
stageNums = [0, 1, 2, 3, 4, 5, 6]
for i in stageNums:
    fileNm = os.path.join(dirNm, 'neural_activity_l' + str(i) + '.csv')
    data = pd.read_csv(fileNm, delimiter = ',')
    print(data.shape)
    acts = data["activity"]
    print('mean neuronal activity for layer {}: {}'.format(i, acts.mean()))
    plt.hist(acts, bins = 100) # bins ask for number of bins (or bars)
    plt.show()
'''
