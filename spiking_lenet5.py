import numpy as np
import os
import torch
import torch.utils.data
import torchvision.transforms as transforms
import torchvision.datasets as datasets
from model import mnist_mlp, lenet5
import torch.nn as nn
import datetime
from spiking import SpikeRelu, poisson_spikes
import h5py

class lenet5_spiking(nn.Module):

    def __init__(self, thresholds):
        super(lenet5_spiking, self).__init__()

        # set-1
        self.conv1 = nn.Conv2d(in_channels=1, out_channels=20, kernel_size=5, padding=2, bias=False)
        self.bin1 = SpikeRelu(thresholds[0], monitor=True, index=(0,0,14,14))
        #self.spike_train = self.bin1.get_spike_train()
        self.avg_pool1 = nn.AvgPool2d(kernel_size=2, stride=2)
        self.avg_spike1 = SpikeRelu(thresholds[1])

        # set-2
        self.conv2 = nn.Conv2d(in_channels=20, out_channels=50, kernel_size=5, padding=2, bias=False)
        self.bin2 = SpikeRelu(thresholds[2])
        self.avg_pool2 = nn.AvgPool2d(kernel_size=2, stride=2)
        self.avg_spike2 = SpikeRelu(thresholds[3])

        # set-3
        self.fc3 = nn.Linear(50*7*7, 500, bias=False)
        self.bin3 = SpikeRelu(thresholds[4])

        # set-4
        self.fc4 = nn.Linear(500, 10, bias=False)
        self.bin4 = SpikeRelu(thresholds[5])


    def forward(self, x):
        x = self.conv1(x)
        x = self.bin1(x)

        x = self.avg_pool1(x)
        x = self.avg_spike1(x)

        x = self.conv2(x)
        x = self.bin2(x)

        x = self.avg_pool2(x)
        x = self.avg_spike2(x)

        x = x.view(-1, 7*7*50)
        x = self.fc3(x)
        x = self.bin3(x)

        x = self.fc4(x)
        x = self.bin4(x)

        return x

    def get_spike_train(self):
        return self.bin1.get_spike_train()

def computeThresholds_generic(model_path, fileNm):
    data_transform = transforms.Compose([
                           transforms.ToTensor(),
                           transforms.Normalize(mean=[0.1307], std=[0.3081])
                           ])
    batch_size = 50
    val_loader = torch.utils.data.DataLoader(datasets.MNIST('./data', train=True, transform=data_transform), batch_size=batch_size, shuffle=False)

    model = lenet5()
    model.load_state_dict(torch.load(os.path.join(model_path, fileNm)))
    model.eval()
    print (model)

    print('Predicting the category for all test images..')
    total_correct = 0
    total_images = 0
    batch_num = 0
    confusion_matrix = np.zeros([10,10], int)
    outputs = []
    if torch.cuda.is_available():
        model.cuda()
    with torch.no_grad():
        for data in val_loader:
            #if batch_num == 1:
            #    break
            images, labels = data
            images = images.cuda()
            labels = labels.cuda()
            outputs = model(images)

            _, predicted = torch.max(outputs.data, 1)
            total_images += labels.size(0)
            total_correct += (predicted == labels).sum().item()
            for i, l in enumerate(labels):
                confusion_matrix[l.item(), predicted[i].item()] += 1

            batch_num += 1

    model_accuracy = total_correct / total_images * 100
    print('Model accuracy on {0} test images: {1:.2f}%'.format(total_images, model_accuracy))
    model.print_max_act()


#def saveData(spikes, t):
def saveData(outFName, spikes, t):
    #outFName = "savedData/mnist_spiking_tw40.h5"
    gpName = "mnist"
    hf = h5py.File(outFName, 'a')
    g = hf.get(gpName)
    if not g:
        g = hf.create_group(gpName)
    dsName = "ts-" + str(t)
    g.create_dataset(dsName, data=spikes)

def createSpikingModel_generic(model_path, fileNm, thresholds):

    model = lenet5()
    model.load_state_dict(torch.load(os.path.join(model_path, fileNm)))
    model.eval()

    new_model = lenet5_spiking(thresholds)

    for m in new_model._modules.keys():
        if isinstance(new_model._modules[m], nn.Conv2d) or isinstance(new_model._modules[m], nn.Linear):
            new_model._modules[m] = model._modules[m]

    #for param in new_model.parameters():
    #    print('min\tmax\t{}\t{}'.format(torch.min(param.data), torch.max(param.data)))

    return new_model


def simulateSpikeModel(model_path, fileNm, thresholds):
    data_transform = transforms.Compose([
                           transforms.ToTensor(),
                           transforms.Normalize(mean=[0.1307], std=[0.3081])
                           ])
    batch_size = 1
    val_loader = torch.utils.data.DataLoader(datasets.MNIST('./data', download=True, train=False, transform=data_transform), batch_size=batch_size, shuffle=False)

    model = lenet5()
    model.load_state_dict(torch.load(os.path.join(model_path, fileNm)))
    model.eval()

    time_window = 40 # ann acc = 97.77% snn acc: ~95%
    total_correct = 0
    total_images = 0
    batch_num = 0
    confusion_matrix = np.zeros([10,10], int)
    out_spikes_t_b_c = torch.zeros((time_window, batch_size, 10))
    #tt, numBatches = 0, int(10000 / batch_size)
    numBatches = 100

    with torch.no_grad():
        #for data in val_loader:
        for i, (images, labels) in enumerate(val_loader):
            if batch_num >= numBatches:
                break
            print ('\n\n------------ inferring batch {} -------------'.format(batch_num))

            #images, labels = data

            spikeModel = createSpikingModel_generic(model_path, fileNm, thresholds)
            sum_in_spikes = torch.zeros(images.size()[1:])
            sum_in_spikes = sum_in_spikes.unsqueeze(0)

            for t in range(time_window):
                #print('shape of input data:', images.shape)

                # convert image pixels to spiking inputs
                spikes = poisson_spikes(images.numpy())
                #saveData(spikes, t)

                # supply random inputs
                spikes = torch.from_numpy(spikes)
                sum_in_spikes += spikes.float()

                out_spikes = spikeModel(spikes.float())
                #print(out_spikes)
                out_spikes_t_b_c[t,:,:] = out_spikes

            approx_pixel = sum_in_spikes[(0,0,14,14)] / time_window * 2.82
            print('actual pixel value: {}\tapprox pixel value {}'.format(images[0,0,14,14], approx_pixel))

            (vmem, spike_train) = spikeModel.get_spike_train()
            approx_act = sum(spike_train) /time_window * 15.131
            #if i in [0, 779, 780, 781]:
            #print('vmem {}\tapprox activation value: {}'.format(vmem, approx_act))

            images = images.to('cuda:0')
            model = model.to('cuda:0')
            model(images.float())
            print('actual activation value: {}\tapprox activation value {}'.format(model.get_act_val((0,0,14,14)), approx_act))

            # accumulating output spikes for all images in a batch
            total_spikes_b_c = torch.zeros((batch_size, 10))
            for b in range(batch_size):
                total_spikes_per_input = torch.zeros((10))
                for t in range(time_window):
                    total_spikes_per_input += out_spikes_t_b_c[t,b,:]
                #print ("total spikes per output: {}".format(total_spikes_per_input / time_window))
                #print ("total spikes per output: {}".format(total_spikes_per_input ))
                total_spikes_b_c[b,:] = total_spikes_per_input
                #total_spikes_b_c[b,:] = total_spikes_per_input / time_window # note the change

            _, predicted = torch.max(total_spikes_b_c.data, 1)
            total_images += labels.size(0)
            total_correct += (predicted == labels).sum().item()
            for i, l in enumerate(labels):
                confusion_matrix[l.item(), predicted[i].item()] += 1
            batch_num += 1
    model_accuracy = total_correct / total_images * 100
    print('Model accuracy on {0} test images: {1:.2f}%'.format(total_images, model_accuracy))

def main():
    model_path = './saved_models/lenet5'
    fileNm = 'lenet5_2.4.pth' # acc: 97.77%
    #computeThresholds_generic(model_path, fileNm)
    #max activations: conv_relu1: 15.130967, avg1: 14.167399, conv_relu2: 38.394875, avg2: 28.443781, fc_relu1: 21.573858, fc2: 33.420769

    # thresholds = 15.131/2.82, 14.167/15.131, 38.395/14.167, 28.44/38.395, 21.57/28.44, 33.42/21.57
    thresholds = [5.365, 0.936, 2.710, 0.741, 0.758, 1.55]
    #thresholds = [5.365, 0.936, 2.710, 0.741, 0.758, 0]
    #createSpikingModel_generic(model_path, fileNm, thresholds)
    simulateSpikeModel(model_path, fileNm, thresholds)

if __name__ == '__main__':
    main()
