import argparse
import os
import shutil
import time
import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.nn.parallel
import torch.backends.cudnn as cudnn
import torch.optim
import torch.utils.data
import torchvision.transforms as transforms
import torchvision.datasets as datasets
import numpy as np


from model import mnist_mlp
from train_routines import *

# used for logging to TensorBoard
#from tensorboard_logger import configure, log_value

parser = argparse.ArgumentParser(description='Binary networks with ternary errors')
parser.add_argument('--epochs', default=100, type=int,
                    help='number of total epochs to run (default:100)')

parser.add_argument('--n-hidden', default=2, type=int,
                    help='number of hidden layers (default:2)')

parser.add_argument('--n-neurons', default=600, type=int,
                    help='number of neurons in hidden layer (default:600)')

parser.add_argument('--n-bits', default=8, type=int,
                    help='number of bits per weight (default:8)')

parser.add_argument('-b', '--batch-size', default=100, type=int,
                    help='mini-batch size (default: 100)')

parser.add_argument('--lr', '--learning-rate', default=0.1, type=float,
                    help='initial learning rate (default:0.1)')

parser.add_argument('--vanilla-network',  action='store_true',
                    help='Use a vanilla network with ReLUs and full-precision errors and weights. (default: False)')

parser.add_argument('--ternary-errors',  action='store_true',
                    help='Use ternary errors (default: False). Only effective if --vanilla-network is not specified.')

parser.add_argument('--bipolar-activations',  action='store_true',
                    help='Use bipolar +1/-1 activations, otherwise, use unipolar +1/0 activations. Only effective if --vanilla-network is not specified. (default: False)')


def main():
    args = parser.parse_args()

    data_transform = transforms.Compose([
                           transforms.RandomRotation(45),
                           transforms.RandomHorizontalFlip(),
                           transforms.RandomVerticalFlip(),
                           transforms.ToTensor(),
                           transforms.Normalize(mean=[0.1307], std=[0.3081])
                           ])
    batch_size = 50
    train_loader = torch.utils.data.DataLoader(datasets.MNIST('./data', train=True, download=True,
                       transform=data_transform),batch_size=batch_size, shuffle=True)
    val_loader = torch.utils.data.DataLoader(datasets.MNIST('./data', train=False, transform=data_transform), batch_size=batch_size, shuffle=False)



    def L1HingeLoss(top_output,labels,hinge = 2**args.n_bits):
        loss = F.relu(top_output - top_output[torch.arange(top_output.size(0)).long(),labels].unsqueeze(1) + hinge)
        return loss.sum(1).mean()

    model = mnist_mlp(args.vanilla_network,args.n_hidden,args.n_neurons,args.n_bits,args.bipolar_activations,args.ternary_errors,0.1,0.1)

    if torch.cuda.is_available():
        model.cuda()
        use_cuda = True
    else:
        use_cuda = False
    print(model)

    criterion =  L1HingeLoss #nn.CrossEntropyLoss().cuda()#
    optimizer = torch.optim.SGD(model.parameters(), args.lr)

    use_cuda = True

    best_avg_prec = 0

    for epoch in range(args.epochs):

        adjust_learning_rate(optimizer, epoch+1,args.lr,args.ternary_errors)

        train(train_loader, model, criterion, optimizer, epoch,use_cuda)

        avg_prec, avg_loss = validate(val_loader, model, criterion, epoch,use_cuda)
        validate(val_loader, model, criterion, epoch,use_cuda)

        if avg_prec > best_avg_prec:
            best_avg_prec = avg_prec

            # Checkpoint the model
            str_val = '%.3f'% best_avg_prec
            fileNm = 'spiking_bnn_'+ str(str_val) + '.pth'
            torch.save({
                'epoch': epoch,
                'model_state_dict': model.state_dict(),
                'optimizer_state_dict': optimizer.state_dict(),
                'loss': avg_loss
                }, './data/spiking_float/'+fileNm)
                #}, './data/spiking_bnn_5bits/'+fileNm)

if __name__ == '__main__':
    main()

