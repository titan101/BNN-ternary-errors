import numpy as np
import os
import torch
import torch.utils.data
import torchvision.transforms as transforms
import torchvision.datasets as datasets
#from mobilenet_trimmed3 import MobileNet_trimmed3, MobileNet_trimmed3_spiking, MobileNet_trimmed3_nobn
from mobilenet_trimmed4 import MobileNet_trimmed4, MobileNet_trimmed4_nobn, MobileNet_trimmed4_spiking
from mobilenet_trimmed5 import MobileNet_trimmed5, MobileNet_trimmed5_nobn, MobileNet_trimmed5_spiking
import torch.nn as nn
import datetime
from spiking import SpikeRelu, poisson_spikes
import h5py
from data_loaders import tinyimagenet_get_datasets
import re
from combined_snn_ann import create_partial_model
from torchsummary import summary

#device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
device = 'cpu'

class Hook():
    def __init__(self, module, backward=False):
        if backward == False:
            self.hook = module.register_forward_hook(self.hook_fn)
        else:
            self.hook = module.register_backward_hook(self.hook_fn)

    def hook_fn(self, module, input, output):
        self.input = input
        self.output = output

    def close(self):
        self.hook.remove()

def adjust_weights(wt_layer, bn_layer):
    #print(wt_layer.weight.size())
    num_out_channels = wt_layer.weight.size()[0]

    bias = torch.zeros(num_out_channels)
    wt_layer_bias = torch.zeros(num_out_channels)
    if wt_layer.bias is not None:
        wt_layer_bias = wt_layer.bias

    wt_cap = torch.zeros(wt_layer.weight.size())
    for i in range(num_out_channels):
        gamma = bn_layer.weight[i]
        beta = bn_layer.bias[i]
        sigma = bn_layer.running_var[i]
        #print(sigma)
        mu = bn_layer.running_mean[i]
        eps = bn_layer.eps
        scale_fac = gamma / torch.sqrt(eps+sigma)
        #wt_cap[i,:,:,:] = wt_layer.weight[i,:,:,:]*gamma / torch.sqrt(eps+sigma)
        #bias[i] = beta - ((mu*gamma) / torch.sqrt(sigma+eps))
        wt_cap[i,:,:,:] = wt_layer.weight[i,:,:,:]*scale_fac
        bias[i] = (wt_layer_bias[i]-mu)*scale_fac + beta
    return (wt_cap, bias)


def merge_bn(model):
    pre_bn_wts = []
    bn_params = []
    fc_wts = []
    for i,m in enumerate(model._modules.keys()):

        if isinstance(model._modules[m], nn.Conv2d):
            pre_bn_wts.append(model._modules[m])

        if isinstance(model._modules[m], nn.BatchNorm2d):
            bn_params.append(model._modules[m])
            print(i, torch.min(model._modules[m].running_var))

        if isinstance(model._modules[m], nn.Linear):
            fc_wts.append(model._modules[m])

    print('#pre_bn_wts: {}\t#bn_params: {}'.format(len(pre_bn_wts), len(bn_params)))
    assert(len(pre_bn_wts) == len(bn_params))

    all_new_wts = []
    for i in range(len(bn_params)):
        all_new_wts.append(adjust_weights(pre_bn_wts[i], bn_params[i]))

    print('length of all_new_wts: {}'.format(len(all_new_wts)))

    new_model = MobileNet_trimmed4_nobn(100)
    #new_model = MobileNet_trimmed5_nobn()
    i = 0
    for param in new_model.parameters():
        print(i, param.data.size())
        if i == 22: # (for mobilenet_trimmed_4)
        #if i == 24: # (for mobilenet_trimmed_5)
            param.data = fc_wts[0].weight
        elif i > 22: # (for mobilenet_trimmed_4)
            param.data = fc_wts[0].bias
        else:
            param.data = all_new_wts[int(i/2)][i%2]
            #break
        i += 1
    return new_model


def computeThresholds_generic(data_dir, model_path, fileNm, num_classes=100):
    train_dataset, val_dataset = tinyimagenet_get_datasets(data_dir)

    batch_size = 50
    # load dataset
    train_loader = torch.utils.data.DataLoader(train_dataset, batch_size=batch_size, shuffle=True)
    val_loader = torch.utils.data.DataLoader(val_dataset, batch_size=batch_size, shuffle=False)

    #model = MobileNet_trimmed5()
    model = MobileNet_trimmed4(num_classes)
    checkpoint = torch.load(os.path.join(model_path, fileNm))
    model.load_state_dict(checkpoint['state_dict'])
    model.eval()
    print (model)

    '''
    print('model')
    for m in model.parameters():
        print(torch.min(m.data), torch.max(m.data))
    '''

    new_model = merge_bn(model)
    print(new_model)
    new_model = new_model.to(device)
    torch.save(new_model.state_dict(), './saved_models/mobilenet/mobilenet_trimmed4_52.525.pth.tar')
    #torch.save(new_model.state_dict(), './saved_models/mobilenet/mobilenet_trimmed5.pth.tar')

    '''
    print('new model')
    for m in new_model.parameters():
        print(torch.min(m.data), torch.max(m.data))
    '''

    '''
    print(new_model._modules.get('fc1').weight.size(), new_model._modules.get('fc1').bias.size())
    in_ = torch.zeros((1,3,56,56))
    print(new_model(in_))
    return
    '''
    #for m in model._modules.keys():
    #    if isinstance(model._modules[m], nn.BatchNorm2d):
    #        print('{}: gamma: {}\t beta: {}'.format(m, model._modules[m].weight.size(), model._modules[m].bias.size()))
    #        print('{}: mean: {}\t var: {}'.format(m, model._modules[m].running_mean.size(), model._modules[m].running_var.size()))
    #        #print(model._modules[m].running_mean)

    # register hooks on every layer
    ls = []
    new_model.eval()
    #for m in model.modules():
    for m in new_model.modules():
        #ls.append(m) # apply hooks to ALL layers
        if type(m) == torch.nn.ReLU or type(m) == torch.nn.Linear \
        or type(m) == torch.nn.AvgPool2d or type(m) == torch.nn.BatchNorm2d:
            ls.append(m)

    hookF = [Hook(layer) for layer in ls]
    print('number of forward hooks: {}'.format(len(hookF)))

    print('Predicting the category for all test images..')
    total_correct = 0
    total_images = 0
    batch_num = 0
    #confusion_matrix = np.zeros([200,200], int)
    confusion_matrix = np.zeros([num_classes,num_classes], int)
    outputs = []
    if torch.cuda.is_available():
        model.cuda()
    max_val = torch.zeros(len(hookF)+1)
    max_val = max_val.to(device)
    acts_99 = np.zeros((len(hookF)+1, 10000))
    with torch.no_grad():
        for n, data in enumerate(val_loader):
            #if batch_num >= 1:
            #    break
            images, labels = data

            images = images.cuda()
            #print (images.size())
            labels = labels.cuda()
            outputs = new_model(images)
            #print (outputs)
            #outputs = model(images)

            _, predicted = torch.max(outputs.data, 1)
            #print(predicted, labels)
            total_images += labels.size(0)
            total_correct += (predicted == labels).sum().item()
            for i, l in enumerate(labels):
                confusion_matrix[l.item(), predicted[i].item()] += 1

            max_val[0] = torch.max(max_val[0], torch.max(images))
            img_max = np.amax(images.cpu().numpy(), axis=(1,2,3))
            #print(img_max.shape)
            #acts_99[0][n*batch_size:(n+1)*batch_size] = np.amax(images.cpu().numpy(), axis=(1,2,3))
            acts_99[0,n*batch_size:(n+1)*batch_size] = img_max
            for i, hook in enumerate(hookF):
                if i == len(hookF)-1:
                    acts_99[i+1][n*batch_size:(n+1)*batch_size] = np.amax(hook.output.cpu().numpy(), axis=1)
                else:
                    acts_99[i+1][n*batch_size:(n+1)*batch_size] = np.amax(hook.output.cpu().numpy(), axis=(1,2,3))
                #max_val[i+1] = torch.max(max_val[i+1], torch.max(hook.output))

            #batch_num += 1

    #print(np.percentile(acts_99, 99.9, axis=1))
    max_val = np.percentile(acts_99, 99.9, axis=1)
    model_accuracy = total_correct / total_images * 100
    print('Model accuracy on {0} test images: {1:.2f}%'.format(total_images, model_accuracy))
    print('max activations: ', max_val)
    thresholds = torch.zeros(len(max_val)-1)
    for i in range(len(thresholds)):
        thresholds[i] = max_val[i+1] / max_val[i]
    #np.savetxt('mobnet_thres_t5.txt', thresholds, fmt='%.5f')
    np.savetxt('mobnet_thres_t4.txt', thresholds, fmt='%.5f')
    return thresholds


#def saveData(spikes, t):
def saveData(outFName, spikes, t):
    #outFName = "savedData/mnist_spiking_tw40.h5"
    gpName = "mnist"
    hf = h5py.File(outFName, 'a')
    g = hf.get(gpName)
    if not g:
        g = hf.create_group(gpName)
    dsName = "ts-" + str(t)
    g.create_dataset(dsName, data=spikes)

#def createSpikingModel_generic(model_path, fileNm, thresholds, max_acts):
def createSpikingModel_generic(model_path, fileNm, thresholds, max_acts, clamp_slope=0):

    model = MobileNet_trimmed4_nobn(100)
    #model = MobileNet_trimmed5_nobn()
    checkpoint = torch.load(os.path.join(model_path, fileNm))
    model.load_state_dict(checkpoint) #['state_dict'])
    model.eval()

    new_model = MobileNet_trimmed4_spiking(thresholds, clamp_slope, 100)
    #new_model = MobileNet_trimmed4_spiking(thresholds, 100)
    #new_model = MobileNet_trimmed5_spiking(thresholds)

    for m in new_model._modules.keys():
        if isinstance(new_model._modules[m], nn.Conv2d) or isinstance(new_model._modules[m], nn.Linear):
                #or isinstance(new_model._modules[m], nn.BatchNorm2d):
            #new_model._modules[m] = model._modules[m]
            new_model._modules[m].weight = model._modules[m].weight
            new_model._modules[m].bias = model._modules[m].bias

    i = 0
    for layer in new_model.modules():
        if type(layer) == torch.nn.Conv2d:
            #temp = layer.bias / max_acts[i+1] # when all vth is 1
            temp = layer.bias / max_acts[i]
            layer.bias = torch.nn.Parameter(temp)
            i += 1

    #for param in new_model.parameters():
    #    print('min\tmax\t{}\t{}'.format(torch.min(param.data), torch.max(param.data)))

    '''
    # when all vth is 1
    j = 0
    for layer in new_model.modules():
        if type(layer) == torch.nn.Conv2d or type(layer) == nn.Linear:
            temp = max_acts[j] / max_acts[j+1]
            layer.weight = torch.nn.Parameter(layer.weight * temp)
            j += 1
    '''

    #print (new_model)
    return new_model

def create_buffers(probe_layers):
    mats = []
    with open('map.json') as json_file:
        data = json.load(json_file)
        for p in probe_layers:
            #layer_num = int(p[-1])
            list_num = re.findall("[0-9]", p)
            str_num = ''
            for l in list_num:
                str_num += l
            layer_num = int(str_num)

            shape = data[p]
            temp = np.zeros(shape)
            new_shape = [-1]
            for s in shape:
                new_shape.append(s)
            temp = np.reshape(temp, new_shape)
            #temp = np.expand_dims(temp, axis=0)
            mats.append([temp, layer_num])
    return mats


import json
import matplotlib
import matplotlib.pyplot as plt
matplotlib.style.use('ggplot')

def simulateSpikeModel(data_dir, model_path, fileNm, thresholds, max_acts, probe_layers, spike_probe_layers):

    train_dataset, val_dataset = tinyimagenet_get_datasets(data_dir)
    batch_size = 1
    val_loader = torch.utils.data.DataLoader(val_dataset, batch_size=batch_size, shuffle=True)
    #val_loader = torch.utils.data.DataLoader(val_dataset, batch_size=batch_size, shuffle=False)

    model = MobileNet_trimmed4_nobn(100)
    #model = MobileNet_trimmed5_nobn()
    checkpoint = torch.load(os.path.join(model_path, fileNm))
    model.load_state_dict(checkpoint) #['state_dict']) ## remember this: never set strict=False
    # the weights wont get loaded correctly otherwise
    model.eval()

    # create forward hooks in the original model
    layers = [model._modules.get(name) for name in probe_layers]
    hooks = [Hook(layer) for layer in layers]
    buffers = create_buffers(probe_layers) # has np arrays of shapes of different layer outputs
    # list of tuples of np arrays and layer numbers
    assert(len(buffers) == len(probe_layers))
    #print(len(buffers), len(probe_layers), len(spike_probe_layers))
    #return


    # verify the weights in original and spiking models
    clamp_slope = 6
    layer_nums, weight_mean, weight_var = [], [], []
    spike_model = createSpikingModel_generic(model_path, fileNm, thresholds, max_acts, clamp_slope)
    z = 0
    for i,l in enumerate(model._modules.keys()):
        if type(model._modules[l]) == nn.Conv2d or type(model._modules[l]) == nn.Linear:
            #assert(torch.all(torch.eq(model._modules[l].weight, spike_model._modules[l].weight)))
            with torch.no_grad():
                layer_nums.append(i)
                #weight_mean.append(spike_model._modules[l].weight.mean().cpu().numpy())
                #weight_var.append(spike_model._modules[l].weight.var().cpu().numpy())
                weight_mean.append(model._modules[l].weight.mean().cpu().numpy())
                weight_var.append(model._modules[l].weight.var().cpu().numpy())

    '''
    l1 = plt.plot(layer_nums, weight_mean, 'ro', label='mean')
    l2 = plt.plot(layer_nums, weight_var, 'c^', label='variance')
    plt.xlabel('layer number')
    plt.legend()
    plt.title('weights mean and variance values')
    plt.show()
    plt.close()
    '''

    #check the biases also
    layer_nums, bias_mean, bias_var = [], [], []
    z = 0
    for i,l in enumerate(model._modules.keys()):
        if type(model._modules[l]) == nn.Conv2d:
            #assert(torch.all(torch.eq(model._modules[l].bias / max_acts[z], spike_model._modules[l].bias)))
            with torch.no_grad():
                layer_nums.append(i)
                #bias_mean.append((spike_model._modules[l].bias.mean()))
                #bias_var.append((spike_model._modules[l].bias.var()))
                bias_mean.append(model._modules[l].bias.mean())
                bias_var.append(model._modules[l].bias.var())
            z += 1

    '''
    plt.figure(2)
    l1 = plt.plot(layer_nums, bias_mean, 'go', label='mean')
    l2 = plt.plot(layer_nums, bias_var, 'b^', label='variance')
    plt.xlabel('layer number')
    plt.legend()
    plt.title('bias mean and variance values')
    plt.show()
    '''

    from pprint import pprint
    #pprint(weight_mean)


    time_window = 300
    total_correct = 0
    expected_correct = 0
    combined_model_correct = 0
    total_images = 0
    batch_num = 0
    num_classes = 100
    confusion_matrix = np.zeros([num_classes,num_classes], int)
    out_spikes_t_b_c = torch.zeros((time_window, batch_size, num_classes))
    #tt, numBatches = 0, int(10000 / batch_size)
    numBatches = 20
    image_corr = np.zeros(numBatches*batch_size)
    num_layers = len(buffers)
    layer_corr = np.zeros((num_layers+1, numBatches*batch_size))
    #layer_corr = np.zeros((num_layers, numBatches*batch_size))

    with torch.no_grad():
        for i, data in enumerate(val_loader):
            if batch_num >= numBatches:
                break
            print ('\n\n------------ inferring batch {} -------------'.format(batch_num))

            images, labels = data
            sum_in_spikes = torch.zeros(images.size())

            #spikeModel = createSpikingModel_generic(model_path, fileNm, thresholds, max_acts)
            spikeModel = createSpikingModel_generic(model_path, fileNm, thresholds, max_acts, clamp_slope)
            spikeModel.eval()

            # Create forward hooks for layers specified in probe_layers
            #spike_layers = [spikeModel._modules.get(name) for name in probe_layers]
            spike_layers = [spikeModel._modules.get(name) for name in spike_probe_layers]
            spike_hooks = [Hook(layer) for layer in spike_layers]

            spike_buffers = create_buffers(spike_probe_layers) # has np arrays of shapes of different layer outputs

            # starting of time-stepped spike integration
            for t in range(time_window):

                # convert image pixels to spiking inputs
                spikes = poisson_spikes(images.numpy())

                # supply random inputs
                spikes = torch.from_numpy(spikes)

                # add up all the input spikes for this batch of input
                sum_in_spikes += spikes.float()

                # supplying original analog inputs to the spiking model
                #out_spikes = spikeModel(images.float())

                # supplying spiking inputs to the spiking model
                out_spikes = spikeModel(spikes.float())

                out_spikes_t_b_c[t,:,:] = out_spikes

                # record sum of spikes from intermediate layers
                for z,h in enumerate(spike_hooks):
                    #print (z)
                    layer_out = h.output
                    spike_buffers[z][0] += layer_out.cpu().numpy()

            # end of time-stepped spike integration

            # corresponding analog value of these spikes
            for z in range(len(spike_hooks)):
                layer_num = spike_buffers[z][1]
                #print(spike_buffers[z][0].shape)
                spike_buffers[z][0] = (spike_buffers[z][0] / time_window) * max_acts[layer_num].numpy()

            ############### calling the partial non-spiking model here ##############
            #split_layer_num = 16
            split_layer_num = 14

            model_partial = create_partial_model(split_layer_num, model)
            input_size = spike_buffers[7][0].shape[1:]
            #print (input_size)
            #summary(model_partial.to('cuda'), input_size=input_size)

            #### performing inference on this partial model
            #print(spike_buffers[int(split_layer_num/2)-1][1])
            #assert(spike_buffers[int(split_layer_num/2)-1][1] == int(split_layer_num/2))
            output_partial = model_partial(torch.from_numpy(spike_buffers[int(split_layer_num/2)-1][0]).float())
            #print (output_partial.size())
            _, predicted_partial = torch.max(output_partial.data, 1)

            # this is for layer 0
            # save the correlation coefficients between pixels and input spikes
            B, C, H, W = sum_in_spikes.size()
            for b in range(batch_size):
                sum_in_spikes = np.reshape(sum_in_spikes, (batch_size, C*H*W))
                in_pixels = np.reshape(images, (batch_size, C*H*W))

            for b in range(batch_size):
                corr_in = np.corrcoef(in_pixels[b], sum_in_spikes[b] / time_window * max_acts[0])
                image_corr[i*batch_size+b] = corr_in[0,1]

            '''
            index = (0,0,0,0)
            #index = (0,0)
            approx_pixel = sum_in_spikes[index] / time_window * 2.64
            #print('approx pixel val: {}\tactual pixel val: {}'.format(approx_pixel, images[index]))

            (vmem, spike_train) = spikeModel.get_spike_train()
            approx_act = sum(spike_train) /time_window * 21.56
            #approx_act = sum(spike_train) /time_window * 47.019
            #approx_act = sum(spike_train) /time_window * 48.112
            #approx_act = (sum(spike_train) /time_window) * 3.898
            '''

            # perform inference on the original model to compare
            images = images.to(device)
            model = model.to(device)
            output_org = model(images.float())
            #print(output_org)
            _, predicted_org = torch.max(output_org.data, 1)

            # for all the other layers
            # record outputs of intermediate layers of original model
            for z,h in enumerate(hooks):
                layer_out = h.output
                buffers[z][0] = layer_out.cpu().numpy()


            # this is for all the other layers of the spiking model
            # save the correlation coefficients between spike acts and analog acts
            for l in range(len(hooks)):
                if len(buffers[l][0].shape) > 2:
                    B,C,H,W = buffers[l][0].shape
                    buffers[l][0] = np.reshape(buffers[l][0], (B, C*H*W))
                    spike_buffers[l][0] = np.reshape(spike_buffers[l][0], (B, C*H*W))

            for l in range(len(hooks)):
                for b in range(batch_size):
                    corr_layer = np.corrcoef(buffers[l][0][b], spike_buffers[l][0][b])
                    layer_corr[l][i*batch_size+b] = corr_layer[0,1]


            # accumulating output spikes for all images in a batch
            total_spikes_b_c = torch.zeros((batch_size, num_classes))
            for b in range(batch_size):
                total_spikes_per_input = torch.zeros((num_classes))
                for t in range(time_window):
                    total_spikes_per_input += out_spikes_t_b_c[t,b,:]
                #print ("total spikes per output: {}".format(total_spikes_per_input / time_window))
                print ("total spikes per output: {}".format(total_spikes_per_input ))
                total_spikes_b_c[b,:] = total_spikes_per_input
                #total_spikes_b_c[b,:] = total_spikes_per_input / time_window # note the change

            _, predicted = torch.max(total_spikes_b_c.data, 1)
            total_images += labels.size(0)
            total_correct += (predicted == labels).sum().item()
            expected_correct += (predicted_org.cpu() == labels).sum().item()
            # # correct classifications for combined ann+snn model
            combined_model_correct += (predicted_partial.cpu() == labels).sum().item()

            #if predicted_org.cpu() == labels:
            #    expected_correct += 1
            #print(predicted, predicted_org, labels)
            print('snn: {}\tann: {}\tpart: {}\treal: {}'.format(predicted, predicted_org, predicted_partial, labels))
            for i, l in enumerate(labels):
                confusion_matrix[l.item(), predicted[i].item()] += 1

            #np.save('./correlation_trimmed5/correlation_data_'+str(batch_num), layer_corr)
            batch_num += 1

    model_accuracy = total_correct / total_images * 100
    expected_acc = expected_correct / total_images * 100
    combined_model_acc = combined_model_correct / total_images * 100
    print('Model accuracy on {} test images: {}%\tacc. of ANN: {}%'.format(total_images, model_accuracy, expected_acc))
    print ('Combined model accuracy: {}%'.format(combined_model_acc))
    #print('Model accuracy on {0} test images: {1:.2f}%\tacc. of ANN: {3:.4f}%'.format(total_images, model_accuracy, expected_acc))

    # Plot correlations
    for l in range(len(hooks)):
        plt.plot(layer_corr[l])
    layer_corr[len(hooks)] = image_corr
    #leg = [str(buffers[l][1]) for l in range(len(buffers))]
    #leg.append('0')
    leg = [str(i) for i in range(len(buffers)+1)]
    plt.legend(leg)
    plt.title('Correlation between ANN and SNN activations')
    plt.show()


def main():
    #data_dir = '/i3c/hpcl/sms821/Research/SpikSim/SpikingNN/mobilenet_spiking/datasets/tinyimagenet/tiny-imagenet-200'
    data_dir = '/i3c/hpcl/sms821/Research/SpikSim/SpikingNN/mobilenet_spiking/datasets/tinyimagenet/tiny-imagenet-100'

    #fileNm = 'checkpoint_48.230.pth.tar'
    # max acts: [  2.6400, 125.8301,  32.9018, 146.7865,  62.8477, 327.1807,  29.0922, 151.4538,  52.4680, 278.1843,  32.8772,
    # 88.2568,  73.1872, 253.6885, 31.0710,  86.0400,  32.9477, 152.7047,  23.3248,  75.9956,  47.6600, 80.9032, 342.5485,
    # 13.7243,  42.7046],

    ############### mobilenet trimmed 4 (acc of 30% on 100 images, time window=300 and non spiking last layer) ###############
    model_path = '/i3c/hpcl/sms821/Research/SpikSim/SpikingNN/mobilenet_spiking/saved_models/mobilenet_trimmed4'
    #fileNm = 'checkpoint_52.525.pth.tar'
    fileNm = 'mobilenet_trimmed4_2.6quant.pth.tar'
    #max_acts = torch.Tensor([ 2.6400, 22.4585, 49.2655, 48.1124, 32.7459, 19.8231, 34.7246, 25.8838, 28.0549, \
    #        14.4075, 23.2757, 19.7882,  6.1160, 30.1417])
    # 99.9th percentile of acts

    #computeThresholds_generic(data_dir, model_path, fileNm, 100)

    #max_acts = torch.Tensor([ 2.64, 21.563, 47.019, 41.474, 28.673, 18.045, 28.154, 17.927, 22.112, 12.116, \
    #        19.678, 17.324, 3.898, 25.318])
    #max_acts = torch.Tensor([2.6400001,   4.74851729,  8.99933282,  7.37503538,  8.77585093,  5.56113336,
    #      4.91531538,  4.15225612,  3.8169439,   3.46778221,  4.45621881, 10.24954767,
    #        3.77392667, 18.01635039])
    max_acts = torch.Tensor([ 2.64, 4.734, 8.99, 7.35, 8.78, 5.59, 4.935, 4.15, 3.924, 3.57, 4.45, 10.42, 3.7, 16.966])
    thresholds = np.loadtxt('mobnet_thres_t4.txt')
    model_path = 'saved_models/mobilenet'
    fileNm = 'mobilenet_trimmed4_52.525.pth.tar'
    #fileNm = 'mobilenet_trimmed4.pth.tar'
    ##########################################################################################################################

    ############### mobilenet trimmed 5 (acc of 22% on 50 images, time window=300 and non spiking last layer) ###############
    ##### 100 images, 25% =======
    #model_path = '/i3c/hpcl/sms821/Research/SpikSim/SpikingNN/mobilenet_spiking/saved_models/mobilenet_trimmed5'
    #fileNm = 'checkpoint_37.100.pth.tar'

    #computeThresholds_generic(data_dir, model_path, fileNm, 200)

    ## trimmed 5 (99th percentile of activations)
    ##max_acts = torch.Tensor([2.640, 45.1179, 132.679, 55.643, 36.403, 118.215, 49.220,  51.06, 39.707, 92.139,
    ##           43.407, 34.469, 5.454, 1.775, 7.669])
    #max_acts = torch.Tensor([  2.6400001,   80.346327,   206.4823342,   75.64910452,  56.86582932,
    #     141.00710329,  80.16095164,  79.6418679,   63.80969327, 144.89541226,
    #       70.36809487,  44.98516922,   4.17610022,   1.30551494,   6.0756676 ])
    #thresholds = np.loadtxt('mobnet_thres_t5.txt')
    #model_path = 'saved_models/mobilenet'
    #fileNm = 'mobilenet_trimmed5.pth.tar'
    ##########################################################################################################################

    #thresholds = np.loadtxt('mobnet_thres.txt')
    #thresholds = np.array([1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1])
    #thresholds = np.array([1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1])
    #thresholds = np.array([1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0.30907, 1])
    #createSpikingModel_generic(model_path, fileNm, thresholds, max_acts)
    layer_names = ['relu1', 'relu2', 'relu3', 'relu4', 'relu5', 'relu6', 'relu7', 'relu8', 'relu9', 'relu10', 'relu11', 'avgpool1']
    spike_layer_names = ['relu1', 'relu2', 'relu3', 'relu4', 'relu5', 'relu6', 'relu7', 'relu8', 'relu9', 'relu10', 'relu11', 'relu12']
    simulateSpikeModel(data_dir, model_path, fileNm, thresholds, max_acts, layer_names, spike_layer_names)

if __name__ == '__main__':
    main()
