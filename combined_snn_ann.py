import os
import torch
import torch.nn as nn
from mobilenet_trimmed4 import MobileNet_trimmed4, MobileNet_trimmed4_nobn, MobileNet_trimmed4_spiking

def main():
    model_path = 'saved_models/mobilenet'
    fileNm = 'mobilenet_trimmed4_52.525.pth.tar'
    model = MobileNet_trimmed4_nobn(100)
    checkpoint = torch.load(os.path.join(model_path, fileNm))
    model.load_state_dict(checkpoint) #['state_dict']) ## remember this: never set strict=False
    model.eval()

    create_partial_model(9, model)



class mobilenet_partial(nn.Module):

    def __init__(self, layer_num, layer_num_to_type, org_model):
        super(mobilenet_partial, self).__init__()

        layer_list = []
        last_layer_num = len(layer_num_to_type)
        for l in range(layer_num, last_layer_num-1):
            layer_name = layer_num_to_type[l]
            layer_list.append(org_model._modules.get(layer_name))

        self.layer_stack = nn.Sequential(*layer_list)
        #print (layer_list)
        layer_name = layer_num_to_type[last_layer_num-1]
        self.fc1 = org_model._modules.get(layer_name)

    def forward(self, x):
        x = self.layer_stack(x)

        x = x.view(-1, 1024)
        x = self.fc1(x)

        return x
        #return self.layer_stack(x)


def create_partial_model(split_layer_num, model):
    layer_num = 0
    layer_num_to_type = {}
    for i in model._modules.keys():
        if isinstance(model._modules[i], nn.Conv2d) or \
                isinstance(model._modules[i], nn.ReLU) or \
                isinstance(model._modules[i], nn.Linear) or \
                isinstance(model._modules[i], nn.AvgPool2d):
            #print (layer_num)
            layer_num_to_type[layer_num] = i
            layer_num += 1

    #for i in layer_num_to_type.items():
    #    print (i)

    model_partial = mobilenet_partial(split_layer_num, layer_num_to_type, model)

    last_layer_num = len(layer_num_to_type)

    for l in range(layer_num, last_layer_num):
        layer_name = layer_num_to_type[l]
        module_partial = model_partial._modules.get(layer_name)
        module = model._modules.get(layer_name)
        if isinstance(module, nn.Conv2d) or isinstance(module, nn.Linear):
            module_partial.weight.data = module.weight.data.copy()
            if module.bias.data is not None:
                module_partial.bias.data = module.bias.data.copy()

    return model_partial
    #print (model_partial)

#for param in model_partial.parameters():
#    print(torch.min(param.data), torch.max(param.data))

if __name__=='__main__':
    main()
